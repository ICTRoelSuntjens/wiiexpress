package custom.live.feed;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

/**
 * Created by roelsuntjens on 24-09-15.
 */
public class ArcHandler {
    private float angle = 0;
    private float top = 0;
    private int factor = 1;
    private RectF oval;
    private Paint firstPaint, secondPaint, thirdPaint;
    private Paint paint2;
    private int max = 360 * 3;

    public ArcHandler(RectF rectf) {
        firstPaint = new Paint();
        secondPaint = new Paint();
        thirdPaint = new Paint();
        paint2 = new Paint();
        this.oval = rectf;
        setFirstColor(Color.GREEN);
        setSecondColor(Color.YELLOW);
        setThirdColor(Color.RED);
        setFirstStrokeWidth(10);
        setSecondStrokeWidth(10);
        setFirstStyle(Paint.Style.FILL);
        setSecondStyle(Paint.Style.FILL);
        setTop(-90);
    }


    public void incAngle() {
        incAngle(1);
    }

    public void incAngle(float value) {
        angle = (angle * factor) + value < 360 ? (angle * factor) + value : ((angle * factor) + value) - 360;
    }

    public void decAngle() {
        decAngle(1);
    }

    public void decAngle(float value) {
        angle = (angle * factor) - value > -360 ? (angle * factor) - value : ((angle * factor) - value) + 360;
    }

    public int getFactor() {
        return factor;
    }

    public void setFactor(int value) {
        factor = value;
    }

    public void setColorCircle(int color) {
        paint2.setColor(color);
    }

    public void setTop(float value) {
        top = value;
    }

    public void setAngle(float value) {
        angle = value * factor;
    }

    public void setFirstColor(int color) {
        firstPaint.setColor(color);
    }

    public void setFirstStrokeWidth(int value) {
        firstPaint.setStrokeWidth(value);
    }

    public void setFirstStyle(Paint.Style style) {
        firstPaint.setStyle(style);
    }

    public void setSecondColor(int color) {
        secondPaint.setColor(color);
    }

    public void setSecondStrokeWidth(int value) {
        secondPaint.setStrokeWidth(value);
    }


    public void setSecondStyle(Paint.Style style) {
        secondPaint.setStyle(style);
    }

    public void setThirdColor(int color) {
        thirdPaint.setColor(color);
    }

    public void setThirdStrokeWidth(int value) {
        thirdPaint.setStrokeWidth(value);
    }


    public void setThirdStyle(Paint.Style style) {
        thirdPaint.setStyle(style);
    }

    public float getCenterX() {
        return oval.centerX();
    }

    public float getCenterY() {
        return oval.centerY();
    }

    public float getWidth() {
        return oval.width();
    }

    public float getHeight() {
        return oval.height();
    }

    public void drawArc(Canvas canvas) {
        canvas.drawArc(oval, top, 360, true, paint2);
        canvas.drawArc(oval, top, angle % max, true, firstPaint);
        if (angle > 360) {
            canvas.drawArc(oval, top, (angle - 360) % max, true, secondPaint);
        } else if (angle < -360) {
            canvas.drawArc(oval, top, (angle + 360) % max, true, secondPaint);
        }
        if (angle > 2 * 360) {
            canvas.drawArc(oval, top, (angle - 2 * 360) % max, true, thirdPaint);
        } else if (angle < -2 * 360) {
            canvas.drawArc(oval, top, (angle + 2 * 360) % max, true, thirdPaint);
        }
    }
}
