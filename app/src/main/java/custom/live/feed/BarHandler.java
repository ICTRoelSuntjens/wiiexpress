package custom.live.feed;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by roelsuntjens on 25-09-15.
 */
public class BarHandler {
    private int value, defaultValue, x, y, defaultPosition, min, max, width, height;
    private Rect rectBackground, rectForeground, rectMax, rectMin;
    private Paint background, foreground, maxPaint;
    private boolean maxLimit, minLimit;

    public BarHandler(int x, int y) {
        this(x, y, 20, 100);
    }

    public BarHandler(int x, int y, int width, int height) {
        this(x, y, width, height, 150, 100);
    }

    public BarHandler(int x, int y, int width, int height, int min, int max) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.min = min;
        this.max = max;
        this.defaultValue = (max - min) / 2;
        background = new Paint();
        foreground = new Paint();
        maxPaint = new Paint();
        background.setColor(Color.BLACK);
        background.setStrokeWidth(1);
        background.setStyle(Paint.Style.FILL);
        foreground.setColor(Color.BLUE);
        foreground.setStrokeWidth(1);
        foreground.setStyle(Paint.Style.FILL);
        maxPaint.setColor(Color.RED);
        maxPaint.setStyle(Paint.Style.FILL);
        updateRects();
    }

    public void updateRects() {
        boolean positive = value > (max - defaultValue);
        int spread = 0;
        if (positive) {
            spread = (int) (((double) (height / 2) / defaultValue) * (value - (min + defaultValue)));
        } else {
            spread = (int) (((double) (height / 2) / defaultValue) * ((min + defaultValue) - value));
        }
        rectBackground = new Rect(x - width / 2, y - height / 2, x + width / 2, y + height / 2);
        int left = x - width / 2;
        int right = x + width / 2;
        int top = positive ? y - spread : y;
        int bottom = positive ? y : y + spread;
        rectForeground = new Rect(left, top, right, bottom);
        rectMax = new Rect(left, (y - (height / 2)) - 10, right, y - (height / 2));
        rectMin = new Rect(left, y + (height / 2), right, (y + (height / 2)) + 10);
    }

    public void draw(Canvas canvas) {
        canvas.drawRect(rectBackground, background);
        canvas.drawRect(rectForeground, foreground);
        if (maxLimit)
            canvas.drawRect(rectMax, maxPaint);
        else if (minLimit)
            canvas.drawRect(rectMin, maxPaint);
    }

    public void setBackground(int color) {
        background.setColor(color);
    }

    public void setForeground(int color) {
        foreground.setColor(color);
    }

    public int getMax() {
        return max;
    }

    public void setMax(int value) {
        this.max = value;
        setDefaultValue((getMax() - getMin()) / 2);
    }

    public int getMin() {
        return min;
    }

    public void setMin(int value) {
        this.min = value;
        setDefaultValue((getMax() - getMin()) / 2);
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        minLimit = value < min;
        maxLimit = value > max;
        this.value = minLimit ? min : maxLimit ? max : value;
        updateRects();
    }

    public int getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(int value) {
        this.defaultValue = value;
    }

    public int getX() {
        return x;
    }

    public void setX(int value) {
        this.x = value;
        updateRects();
    }

    public int getY() {
        return y;
    }

    public void setY(int value) {
        this.y = value;
        updateRects();
    }

    public void setWidth(int value) {
        this.width = value;
        updateRects();
    }

    public void setHeight(int value) {
        this.height = value;
        updateRects();
    }
}
