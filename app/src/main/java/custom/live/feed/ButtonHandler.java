package custom.live.feed;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by roelsuntjens on 24-09-15.
 */
public class ButtonHandler {
    private String buttonName;
    private boolean buttonPressed = false;
    private Rect rectangle, rectanglePressed;
    private Paint rectpaint, pressedpaint, textpaint;

    public ButtonHandler(Rect rect) {
        this(rect, "");
    }

    public ButtonHandler(float x, float y, String name) {
        this((int) x, (int) y, name);
    }

    public ButtonHandler(int x, int y, String name) {
        this(x, y, 50, 50, name);
    }

    public ButtonHandler(int x, int y, int width, int height, String name) {
        this(null, name);
        Rect rect = new Rect();
        rect.set(x - width / 2, y - height / 2, x + width / 2, y + height / 2);
        setRectangle(rect);
    }

    public ButtonHandler(Rect rect, String name) {
        buttonName = name;
        if (rect != null)
            setRectangle(rect);
        rectpaint = new Paint();
        pressedpaint = new Paint();
        textpaint = new Paint();
        setRectColor(Color.LTGRAY);
        setPressedColor(Color.YELLOW);
        setTextColor(Color.BLACK);
        setRectStrokeWidth(5);
        setPressedStrokeWidth(5);
        setTextStrokeWidth(1);
        setRectStyle(Paint.Style.FILL);
        setPressedStyle(Paint.Style.FILL);
        setTextStyle(Paint.Style.FILL_AND_STROKE);
    }

    public boolean contains(int x, int y) {
        return rectangle.contains(x, y);
    }

    public boolean contains(float x, float y) {
        return contains((int) x, (int) y);
    }

    private void setRectangle(Rect rect) {
        if (rect != null) {
            rectangle = rect;
            rectanglePressed = new Rect(rect.left - 1, rect.top - 1, rect.right + 1, rect.bottom + 1);
        }
    }

    public void setTextColor(int color) {
        textpaint.setColor(color);
    }

    public void setPressedColor(int color) {
        pressedpaint.setColor(color);
    }

    public void setRectColor(int color) {
        rectpaint.setColor(color);
    }

    public void setRectStrokeWidth(int value) {
        rectpaint.setStrokeWidth(value);
    }

    public void setPressedStrokeWidth(int value) {
        pressedpaint.setStrokeWidth(value);
    }

    public void setTextStrokeWidth(int value) {
        textpaint.setStrokeWidth(value);
    }

    public void setRectStyle(Paint.Style style) {
        rectpaint.setStyle(style);
    }

    public void setPressedStyle(Paint.Style style) {
        pressedpaint.setStyle(style);
    }

    public void setTextStyle(Paint.Style style) {
        textpaint.setStyle(style);
    }

    public boolean isButtonPressed() {
        return buttonPressed;
    }

    public void setButtonPressed(boolean result) {
        buttonPressed = result;
    }

    public String getButtonName() {
        return buttonName;
    }

    public void setButtonName(String name) {
        buttonName = name;
    }

    public int getCenterX() {
        return rectangle.centerX();
    }

    public int getCenterY() {
        return rectangle.centerY();
    }

    public int getWidth() {
        return rectangle.width();
    }

    public int getHeight() {
        return rectangle.height();
    }

    public void drawButton(Canvas canvas) {
        canvas.drawRect(rectangle, rectpaint);
        if (buttonPressed)
            canvas.drawRect(rectanglePressed, pressedpaint);
        canvas.drawText(buttonName, rectangle.centerX() - buttonName.length() * (textpaint.getTextSize()/3), rectangle.centerY(), textpaint);
    }
}
