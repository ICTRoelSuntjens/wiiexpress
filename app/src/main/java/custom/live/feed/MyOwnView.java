package custom.live.feed;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.MotionEvent;
import android.view.View;

import com.example.roelsuntjens.wiiexpress.LiveFeedActivity;

import model.EikData;
import model.WiiData;

/**
 * Created by roelsuntjens on 23-09-15.
 */
public class MyOwnView extends View {
    public static final int HORIZONTAL = 1, VERTICAL = 2;
    public static String FEEDBACK = "";
    private ArcHandler arcx, arcy, arcz;
    private ButtonHandler b1, b2, ba, bb, bmin, bhome, bplus, bup, bdown, bleft, bright;
    private BarHandler barx, bary, barz;
    private WiiData latestWiiData;
    private EikData latestEikData;
    private RectF ovalx = new RectF(), ovaly = new RectF(), ovalz = new RectF();
    private int orientation;
    private float posX, posY;
    private ButtonHandler startButton, stopButton, bizzyButton;
    private boolean bizzy = false, calibrated = false;
    private long pastFlikkerTime;
    private LiveFeedActivity liveFeedActivity;

    private Rect wiimote;
    private Paint wiimotePaint;

    public MyOwnView(Context context) {
        super(context);
        this.liveFeedActivity = (LiveFeedActivity) context;
        this.latestEikData = new EikData();
        latestEikData.eikx = 118;
        latestEikData.eiky = 118;
        latestEikData.eikz = 140;
        init();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float eventX = event.getX();
        float eventY = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (startButton.contains(eventX, eventY))
                    startPressed();
                if (stopButton.contains(eventX, eventY))
                    stopPressed();
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                startButton.setButtonPressed(false);
                stopButton.setButtonPressed(false);
                break;
            default:
                return false;
        }
        return true;
    }

    private void init() {
        posX = 600;
        posY = 400;
        orientation = VERTICAL;
        initButtonHandlers();
        initArcHandlers();
        initBarHandlers();
    }


    private void initButtonHandlers() {
        int posx = (int) (posX - 450);
        int posy = (int) (posY - 300);
        bup = new ButtonHandler(posx, posy, "|");
        bdown = new ButtonHandler(posx, posy + 100, "|");
        bleft = new ButtonHandler(posx - 50, posy + 50, "--");
        bright = new ButtonHandler(posx + 50, posy + 50, "--");
        ba = new ButtonHandler(posx, posy + 200, "A");
        bb = new ButtonHandler(posx + 200, posy + 150, "B");
        bmin = new ButtonHandler(posx - 50, posy + 300, "-");
        bhome = new ButtonHandler(posx, posy + 300, "#");
        bplus = new ButtonHandler(posx + 50, posy + 300, "+");
        b1 = new ButtonHandler(posx, posy + 450, "1");
        b2 = new ButtonHandler(posx, posy + 550, "2");
        wiimote = new Rect(posx - 100, posy - 50, posx + 100, posy + 650);
        wiimotePaint = new Paint();
        wiimotePaint.setStyle(Paint.Style.STROKE);
        wiimotePaint.setColor(Color.BLACK);
        wiimotePaint.setStrokeWidth(3);

        startButton = new ButtonHandler(wiimote.left + 50, wiimote.bottom + 100, 100, 50, "Start");
        stopButton = new ButtonHandler(startButton.getCenterX() + startButton.getWidth() + 10, startButton.getCenterY(), 100, 50, "Stop");
        bizzyButton = new ButtonHandler(stopButton.getCenterX() + stopButton.getWidth() + 10, stopButton.getCenterY(), "");
        startButton.setPressedColor(Color.GRAY);
        stopButton.setPressedColor(Color.GRAY);
        bizzyButton.setPressedColor(Color.GREEN);
    }

    private void initArcHandlers() {
        if (orientation == VERTICAL) {
            ovalx.set(posX - 150, posY - 400, posX + 150, posY - 100);
            ovaly.set(posX - 150, posY - 050, posX + 150, posY + 250);
            ovalz.set(posX - 150, posY + 300, posX + 150, posY + 600);
        } else if (orientation == HORIZONTAL) {

        }
        // TODO: Make smooth animator
        arcx = new ArcHandler(ovalx);
        arcy = new ArcHandler(ovaly);
        arcz = new ArcHandler(ovalz);
        int fact = 5;
        arcx.setFactor(fact);
        arcy.setFactor(fact);
        arcz.setFactor(fact);
    }

    private void initBarHandlers() {
        barx = new BarHandler((int) (arcx.getCenterX() + arcx.getWidth() / 2 + 25), (int) (arcx.getCenterY()), 25, 150);
        bary = new BarHandler((int) (arcy.getCenterX() + arcy.getWidth() / 2 + 25), (int) (arcy.getCenterY()), 25, 150);
        barz = new BarHandler((int) (arcz.getCenterX() + arcz.getWidth() / 2 + 25), (int) (arcz.getCenterY()), 25, 150);
    }

    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }


    public void setEikData(EikData data) {
        this.latestEikData = data;
        calibrate();
    }

    public void updateBizzyLed(boolean updateMethod) {
        if (bizzy) {
            bizzyButton.setButtonPressed(bizzyButton.isButtonPressed() ? false : true);
        }
        if(!updateMethod)
            ((View) this).invalidate();
    }

    public void update(WiiData latestWiiData) {
        this.latestWiiData = latestWiiData;
        updateBizzyLed(true);
        if (latestEikData != null && latestWiiData != null) {
            arcx.setAngle(latestEikData.eikx - latestWiiData.accx);
            arcy.setAngle(latestEikData.eiky - latestWiiData.accy);
            arcz.setAngle(latestEikData.eikz - latestWiiData.accz);
        } else if (latestEikData == null && latestWiiData != null) {
            arcx.setAngle(latestWiiData.accx);
            arcy.setAngle(latestWiiData.accy);
            arcz.setAngle(latestWiiData.accz);
        }
        if (latestWiiData != null) {
            barx.setValue(latestWiiData.accx);
            bary.setValue(latestWiiData.accy);
            barz.setValue(latestWiiData.accz);
            b1.setButtonPressed(latestWiiData.button1 == 1);
            b2.setButtonPressed(latestWiiData.button2 == 1);
            ba.setButtonPressed(latestWiiData.buttonA == 1);
            bb.setButtonPressed(latestWiiData.buttonB == 1);
            bmin.setButtonPressed(latestWiiData.buttonMin == 1);
            bhome.setButtonPressed(latestWiiData.buttonHome == 1);
            bplus.setButtonPressed(latestWiiData.buttonPlus == 1);
            bup.setButtonPressed(latestWiiData.buttonUp == 1);
            bdown.setButtonPressed(latestWiiData.buttonDown == 1);
            bleft.setButtonPressed(latestWiiData.buttonLeft == 1);
            bright.setButtonPressed(latestWiiData.buttonRight == 1);
        }
        ((View) this).invalidate();
    }

    private void calibrate() {
        if (latestEikData != null) {
            calibrated = true;
            int factor = (latestEikData.eikz - latestEikData.eikx);
            barz.setMax(latestEikData.eikz);
            barz.setDefaultValue(latestEikData.eikz - factor);
            barz.setMin(latestEikData.eikz - factor * 2);
            barx.setMax(latestEikData.eikx + factor);
            barx.setDefaultValue(latestEikData.eikx);
            barx.setMin(latestEikData.eikx - factor);
            bary.setMax(latestEikData.eiky + factor);
            bary.setDefaultValue(latestEikData.eiky);
            bary.setMin(latestEikData.eiky - factor);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        wiimotePaint.setStrokeWidth(3);

        b1.drawButton(canvas);
        b2.drawButton(canvas);
        ba.drawButton(canvas);
        bb.drawButton(canvas);
        bmin.drawButton(canvas);
        bhome.drawButton(canvas);
        bplus.drawButton(canvas);
        bup.drawButton(canvas);
        bdown.drawButton(canvas);
        bleft.drawButton(canvas);
        bright.drawButton(canvas);

        canvas.drawCircle(bb.getCenterX(), bb.getCenterY(), bb.getWidth(), wiimotePaint);
        canvas.drawLine(bb.getCenterX() - bb.getWidth(), bb.getCenterY(), wiimote.right, ba.getCenterY() - ba.getHeight() / 2, wiimotePaint);
        canvas.drawRect(wiimote, wiimotePaint);

        arcx.drawArc(canvas);
        arcy.drawArc(canvas);
        arcz.drawArc(canvas);
        barx.draw(canvas);
        bary.draw(canvas);
        barz.draw(canvas);
        wiimotePaint.setStrokeWidth(1);
        canvas.drawText("X acc:", arcx.getCenterX() - (float) (arcx.getWidth() / 1.5), arcx.getCenterY(), wiimotePaint);
        canvas.drawText("Y acc:", arcy.getCenterX() - (float) (arcy.getWidth() / 1.5), arcy.getCenterY(), wiimotePaint);
        canvas.drawText("Z acc:", arcz.getCenterX() - (float) (arcz.getWidth() / 1.5), arcz.getCenterY(), wiimotePaint);

        startButton.drawButton(canvas);
        stopButton.drawButton(canvas);
        bizzyButton.drawButton(canvas);

        canvas.drawText(FEEDBACK, 0, 0, 50, startButton.getCenterY() + startButton.getHeight() * 2, wiimotePaint);
    }

    private void startPressed() {
        startButton.setButtonPressed(true);
        bizzy = true;
        liveFeedActivity.liveOnClick();
    }

    private void stopPressed() {
        stopButton.setButtonPressed(true);
        bizzy = false;
        bizzyButton.setButtonPressed(false);
        liveFeedActivity.livePauzedOnClick();
    }
}

