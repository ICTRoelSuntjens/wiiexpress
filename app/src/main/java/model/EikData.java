package model;

/**
 * Created by roelsuntjens on 23-09-15.
 */
public class EikData {
    public int eikx;
    public int eiky;
    public int eikz;

    public String objectId;
    public String createdAt;
    public String updatedAt;
}
