package model;

/**
 * Created by roelsuntjens on 18-09-15.
 */
public class AboutData {
    public String context;
    public int columnNr;
    public String updatedAt;
    public String createdAt;
    public String objectId;

    @Override
    public String toString() {
        return "[" + columnNr + "] - " + context;
    }
}
