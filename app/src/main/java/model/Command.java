package model;

/**
 * Created by roelsuntjens on 09-10-15.
 */

public abstract class Command {
    public abstract void execute();
}
