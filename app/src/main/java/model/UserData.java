package model;

/**
 * Created by roelsuntjens on 18-09-15.
 */

public class UserData {
    public String objectId;
    public String createdAt;
    public String updatedAt;
    public String firstname;
    public String lastname;
    public String username;
    public String password;
    public int acces;
    public boolean editable;

    public UserData() {
    }

    public UserData(String firstname, String lastname, String username, String password, int acces) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.password = password;
        this.acces = acces;
        this.editable = true;
    }

    @Override
    public String toString() {
        return "@UserData:  [" + firstname + "][" + lastname + "][" + username + "][" + password + "](" + acces + "), {" + objectId + "}, {" + createdAt + "}, {" + updatedAt + "}";
    }
}
