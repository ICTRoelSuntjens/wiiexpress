package model;

import java.util.ArrayList;

/**
 * Created by roelsuntjens on 09-10-15.
 */
public class PersonResponse {
    public ArrayList<Person> results;

    public class Person {
        public int age;
        public int length;
        public Boots boots;
        public Gloves gloves;
        public Sword sword;
        public Shield shield;

        public String objectId;
        public String updatedAt;
        public String createdAt;
    }

    public class Boots extends Armor {
        public Boots() {
            super("Boots");
        }

        @Override
        public void Reset() {
            size = 44;
            kind = "leather";
            def = 2;
            off = 1;
            dur = 10;
        }
    }

    public class Gloves extends Armor {
        public Gloves() {
            super("Gloves");
        }

        @Override
        public void Reset() {
            size = 10;
            kind = "leather";
            def = 2;
            off = 1;
            dur = 10;
        }
    }

    public class Sword extends Armor {
        public Sword() {
            super("Sword");
        }

        @Override
        public void Reset() {
            size = 1;
            kind = "wood";
            off = 5;
            def = 1;
            dur = 20;
        }
    }

    public class Shield extends Armor {
        public Shield() {
            super("Shield");
        }

        @Override
        public void Reset() {
            size = 1;
            kind = "wood";
            def = 5;
            off = 1;
            dur = 20;
        }
    }

    public abstract class Armor {
        public int size;
        public String kind;
        public int def;
        public int off;
        public int dur;
        private String armor = "?";

        public Armor(String armor) {
            this.armor = armor;
            Reset();
        }

        public abstract void Reset();

        public void print() {
            System.out.println(armor + " size=" + size + " kind=" + kind + " def=" + def + " off=" + off + " dur=" + dur);
        }
    }
}