package View3D.ShootingShip;

/**
 * Created by roelsuntjens on 30-09-15.
 */
public class BulletObject extends Triangle {
    private float speed = 0;
    private float acc = 0;

    public BulletObject(float scale, float x, float y, float z, float red, float green, float blue) {
        super(scale, x, y, z, red, green, blue);
    }

    public float getAcc() {
        return acc;
    }

    public void setAcc(float value) {
        this.acc = value;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float value) {
        this.speed = value;
    }
}
