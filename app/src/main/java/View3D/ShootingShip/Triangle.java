/**
 *
 */
package View3D.ShootingShip;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

/**
 * @author Roel Suntjens
 */
public class Triangle {
    private boolean updated = false;
    private FloatBuffer vertexBuffer;
    private float base = 1.0f;
    private float red, green, blue;
    private float vertices[] = {
            -0.5f, -0.5f, 0.0f,        // V1 - first vertex (x,y,z)
            0.5f, -0.5f, 0.0f,        // V2 - second vertex
            0.0f, 0.5f, 0.0f         // V3 - third vertex
    };
    private float x, y, z;

    public Triangle(float scale, float red, float green, float blue) {
        this(scale, 0, 0, 0, red, green, blue);
    }

    public Triangle(float scale, float x, float y, float z, float red, float green, float blue) {
        vertices = new float[]{
                -base * scale, -base * scale, 0.0f, // V1 - first vertex
                base * scale, -base * scale, 0.0f, // V2 - second vertex
                0.0f, base * scale, 0.0f          // V3 - third vertex
        };
        this.x = x;
        this.y = y;
        this.z = z;
        this.red = red;
        this.green = green;
        this.blue = blue;
        updateVertexBuffer();
    }


    public void draw(GL10 gl) {
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        // set the colour for the triangle
        gl.glColor4f(red, green, blue, 0.5f);
        // Point to our vertex buffer
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
        // Draw the vertices as triangle strip
        gl.glDrawArrays(GL10.GL_TRIANGLES, 0, vertices.length / 3);
        // Disable the client state before leaving
        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
    }

    public void move(float x, float y, float z) {
        int i = 0;
//        vertices = new float[]{
//                vertices[i++] + x, vertices[i++] + y, vertices[i++] + z,
//                vertices[i++] + x, vertices[i++] + y, vertices[i++] + z,
//                vertices[i++] + x, vertices[i++] + y, vertices[i++] + z,
//        };
        this.x = this.x + x;
        this.y = this.y + y;
        this.z = this.z + z;
        updateVertexBuffer();
    }

    private void updateVertexBuffer() {
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(3 * 3 * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        vertexBuffer = byteBuffer.asFloatBuffer();
        vertexBuffer.put(vertices);
        vertexBuffer.flip();
    }

    public float[] getX() {
        return new float[]{vertices[0], vertices[3], vertices[6]};
    }

    public void setX(float value) {
        this.x = value;
    }

    public float[] getY() {
        return new float[]{vertices[1], vertices[4], vertices[7]};
    }

    public void setY(float value) {
        this.y = value;
    }

    public float[] getZ() {
        return new float[]{vertices[2], vertices[5], vertices[8]};
    }

    public void setZ(float value) {
        this.z = value;
    }

    public float X() {
        return x;
    }

    public float Y() {
        return y;
    }

    public float Z() {
        return z;
    }

    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean result) {
        this.updated = result;
    }
}