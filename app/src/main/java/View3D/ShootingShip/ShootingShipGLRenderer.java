package View3D.ShootingShip;

import android.opengl.GLU;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import View3D.GLRenderer;

/**
 * Created by roelsuntjens on 30-09-15.
 */
public class ShootingShipGLRenderer extends GLRenderer {
    private static Lock triangleLock = new ReentrantLock();
    private List<Triangle> triangleList;
    private Triangle updateTriangle;
    private int width, height;
    private long pastBulletATime = 0, pastBulletBTime = 0, shootADelay = 500, shootBDelay = 500;

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        Log.d("GLRenderer", "Surface changed. Width=" + width + " Height=" + height);
        this.width = width;
        this.height = height;
        triangleList = new ArrayList<Triangle>();
        triangleList.add(new Triangle(0.5f, 0, 0, 0, 0, 0, 1));
//        triangleList.add(new Triangle(0.5f, 0, 0, -0.5f, 0, 1, 0));
        updateTriangle = triangleList.get(0);
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();
        GLU.gluPerspective(gl, 45.0f, (float) width / (float) height, 0.1f, 100.0f);
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        Log.d("GLRenderer", "Surface created");
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        gl.glClearColor(0.0f, 0.0f, 0.0f, 1f);
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        gl.glLoadIdentity();
        gl.glTranslatef(0.0f, 0.0f, -5.0f);
        triangleLock.lock();
        try {
            for (int i = 0; i < triangleList.size(); i++) {
                gl.glTranslatef(triangleList.get(i).X(), triangleList.get(i).Y(), triangleList.get(i).Z());
                triangleList.get(i).draw(gl);
                gl.glTranslatef(-triangleList.get(i).X(), -triangleList.get(i).Y(), -triangleList.get(i).Z());
            }
        } finally {
            triangleLock.unlock();
        }
    }

    public void update(long currentTime) {
        if (updateTriangle != null && eikData != null && wiiData != null) {
            float movex = 0, movey = 0, movez = 0.000f;
            float div = 300;
            movex = (((float) wiiData.accx - (float) eikData.eikx) / div);
            movey = (((float) wiiData.accy - (float) eikData.eiky) / div);
            updateTriangle.move(movex, movey, movez);
            if (wiiData.buttonA == 1)
                if (pastBulletATime + shootADelay < currentTime)
                    addBulletA();
            if (wiiData.buttonB == 1)
                if (pastBulletBTime + shootBDelay < currentTime)
                    addBulletB();
            int outx = OutOfTheAreaX(), outy = OutOfTheAreaY();
            if (outx != 0) {
                updateTriangle.setX(outx == 1 ? screenX2 : screenX1);
            }
            if (outy != 0) {
                updateTriangle.setY(outy == 1 ? screenY2 : screenY1);
            }
        }
        boolean found = true;
        triangleLock.lock();
        try {
            while (found) {
                found = false;
                if (triangleList != null) {
                    for (int i = 1; i < triangleList.size(); i++) {
                        if (!triangleList.get(i).isUpdated()) {
                            if (triangleList.get(i).Y() < 2f) {
                                if (triangleList.get(i) instanceof BulletObject) {
                                    BulletObject obj = (BulletObject) triangleList.get(i);
                                    obj.setSpeed(obj.getSpeed() + obj.getAcc());
                                    obj.move(0, obj.getSpeed(), 0);
                                    obj.setZ((float) Math.sin(obj.Y()));
                                } else {
                                    triangleList.get(i).move(0, 0.001f, 0);
                                }
                                triangleList.get(i).setUpdated(true);
                            } else {
                                if (triangleList.get(i) != updateTriangle) {
                                    triangleList.remove(i);
                                    continue;
                                }
                            }
                        }
                        if (i != 0 && triangleList.get(i - 1).Z() > triangleList.get(i).Z()) {
                            if (triangleList.get(i - 1) != updateTriangle) {
                                // latest in the list gets on top
                                // if previous higher the current. Switch
                                Triangle temp = triangleList.get(i - 1);
                                triangleList.set(i - 1, triangleList.get(i));
                                triangleList.set(i, temp);
                                found = true;
                            }
                        }
                    }
                }
            }
            if (triangleList != null)
                for (int i = 0; i < triangleList.size(); i++)
                    triangleList.get(i).setUpdated(false);
        } finally {
            triangleLock.unlock();
        }

    }

    public int OutOfTheAreaX() {
        return updateTriangle.X() < screenX1 ? -1 : updateTriangle.X() > screenX2 ? 1 : 0;
    }

    public int OutOfTheAreaY() {
        return updateTriangle.Y() < screenY1 ? -1 : updateTriangle.Y() > screenY2 ? 1 : 0;
    }


    public void addBulletA() {
        BulletObject bullet = new BulletObject(0.1f, updateTriangle.X(), updateTriangle.Y(), updateTriangle.Z(), 1, 0, 0);
        bullet.setSpeed(0.01f);
        bullet.setAcc(0.0001f);
        triangleList.add(bullet);
    }

    public void addBulletB() {
        BulletObject bullet = new BulletObject(0.1f, updateTriangle.X(), updateTriangle.Y(), updateTriangle.Z(), 0, 1, 0);
        bullet.setSpeed(0.01f);
        bullet.setAcc(0.0005f);
        triangleList.add(bullet);
    }
}