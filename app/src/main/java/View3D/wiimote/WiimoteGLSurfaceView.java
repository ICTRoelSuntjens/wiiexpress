package View3D.wiimote;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;
import android.widget.Toast;

import com.example.roelsuntjens.wiiexpress.Wiimote3DActivity;

import java.util.ArrayList;
import java.util.List;

import API.service;
import View3D.WiimoteGLRenderer;
import model.EikData;
import model.EikDataResponse;
import model.WiiData;
import model.WiiDataResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by roelsuntjens on 05-10-15.
 */
public class WiimoteGLSurfaceView extends GLSurfaceView {
    private static final String NOSERVER = "Could not connect to the server";
    private final float TOUCH_SCALE_FACTOR = 180.0f / 320.0f;
    private List<ToastMessage> toastMessages = new ArrayList<ToastMessage>();
    private float previousX, previousY;
    private WiimoteGLRenderer renderer;
    private Thread updateThread = null;
    private WiiData latestWiiData = null;
    private EikData latestEikData = null;
    private long pastEikTime = 0, eikDelay = 10 * 1000;
    private long pastWiiTime = 0, wiiDelay = 15 * 1000;
    private boolean keepUpdating = true;
    private int keepUpdatingCounter = 0;
    private Wiimote3DActivity app;

    public WiimoteGLSurfaceView(Context context, Wiimote3DActivity app) {
        super(context);
        this.app = app;
        renderer = new WiimoteGLRenderer(context);
        this.setRenderer(renderer);
        this.requestFocus();
        this.setFocusableInTouchMode(true);
        init();
        startUpdating();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float currentX = event.getX();
        float currentY = event.getY();
        float deltaX, deltaY;
        switch (event.getAction()) {
            case MotionEvent.ACTION_MOVE:
                deltaX = currentX - previousX;
                deltaY = currentY - previousY;
                renderer.speedX += (deltaY * TOUCH_SCALE_FACTOR) / 10;
                renderer.speedY += (deltaX * TOUCH_SCALE_FACTOR) / 10;
                break;
        }
        previousX = currentX;
        previousY = currentY;
        return true;
    }

    private void init() {
    }

    private void initUpdateThread() {
        updateThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (true) {
                        long currentTime = System.currentTimeMillis();
                        if ((keepUpdating) || (pastWiiTime + wiiDelay < currentTime && !keepUpdating)) {
                            if (pastWiiTime + wiiDelay < currentTime && !keepUpdating) {
                                showToastMessages();
                            }
                            pastWiiTime = currentTime;
//                            System.out.println("Check for wiidata");
                            update(currentTime);
                        }
                        Thread.sleep(1000 / 10);
                    }
                } catch (InterruptedException e) {
                    System.out.println("WiimoteSurface -> updatethread interrupted");
                }
            }
        });
    }

    private void showToastMessages() {
        for (int i = toastMessages.size() - 1; i > 0; i--) {
            app.showToast(toastMessages.get(i).message, toastMessages.get(i).time);
            toastMessages.remove(i);
        }
    }

    private void update(long currentTime) {
        if (pastEikTime + eikDelay < currentTime) {
            pastEikTime = currentTime;
            service.getInstace().getLatestEikData(new Callback<EikDataResponse>() {
                @Override
                public void success(EikDataResponse eikDataResponse, Response response) {
                    if (eikDataResponse.results.size() > 0) {
                        latestEikData = eikDataResponse.results.get(0);
                    } else {
                        System.out.println("Eikdata not found");
                        toastMessages.add(new ToastMessage("Eik your wiimode", Toast.LENGTH_LONG));
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    latestEikData = null;
                    toastMessages.add(new ToastMessage(NOSERVER, Toast.LENGTH_LONG));
                    System.out.println("Eikdata failed");
                }
            });
        }
        service.getInstace().getLatestData(new Callback<WiiDataResponse>() {
            @Override
            public void success(WiiDataResponse wiiDataResponse, Response response) {
                checkLatestWiiData(wiiDataResponse.results.size() == 0 ? null : wiiDataResponse.results.get(0), latestWiiData);
                if (wiiDataResponse.results.size() > 0) {
                    if (keepUpdating) {
                        latestWiiData = wiiDataResponse.results.get(0);
                        if (latestEikData != null && latestWiiData != null) {
                            float anglex = (float) (((double) 90 / (double) 24) * (latestEikData.eiky - latestWiiData.accy));
                            float angley = (float) (((double) 90 / (double) 24) * (latestEikData.eikx - latestWiiData.accx));
                            float anglez = (latestEikData.eikz - latestWiiData.accz);
                            // x >= 24 && x <= -24
                            // y >= 24 && y <= -24
                            // z >= 00 && z <= 48
                            // z > 24 = upside down
                            renderer.setUpsideDown(anglez > 24);
                            renderer.setUpsideX(renderer.isUpsideDown() && ((anglex > 85 || anglex < -85) || renderer.isUpsideX()));
                            renderer.setUpsideY(renderer.isUpsideDown() && ((angley > 85 || angley < -85) || renderer.isUpsideY()));
                            if (renderer.isUpsideX())
                                anglex = 90 + (90 - anglex);
                            if (renderer.isUpsideY())
                                angley = 90 + (90 - angley);
                            renderer.angleX = anglex;
                            renderer.angleY = -angley;
                            renderer.angleZ = anglez;
                            updateWiiButtons();
                        }
                    }
                } else {
                    latestWiiData = null;
                    System.out.println("Wiidata not found!");
                    toastMessages.add(new ToastMessage("Make first wiidata", Toast.LENGTH_SHORT));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                System.out.println("Wiidata failed");
                toastMessages.add(new ToastMessage(NOSERVER, Toast.LENGTH_LONG));
            }
        });
    }

    private void checkLatestWiiData(WiiData newData, WiiData oldData) {
        if (oldData != null && newData != null) {
            if (newData.objectId.equals(oldData.objectId)) {
                keepUpdatingCounter++;
            } else {
                keepUpdatingCounter = 0;
                keepUpdating = true;
                renderer.speedX = 0;
                renderer.speedY = 0;
                renderer.speedZ = 0;
            }
        } else {
            keepUpdatingCounter++;
        }
        if (keepUpdatingCounter >= 50 || (!keepUpdating && keepUpdatingCounter >= 3)) {
            keepUpdating = false;
            keepUpdatingCounter = 0;
            renderer.angleX = 0;
            renderer.angleY = 0; // 95;
            renderer.angleZ = 0;
            renderer.speedX = 0;
            renderer.speedY = -0.1f;
            renderer.speedZ = 0;
            System.out.println("Stopped Updating!");
        }
    }

    private void updateWiiButtons() {
        renderer.wiiButtonA.setPressed(latestWiiData.buttonA);
        renderer.wiiButtonB.setPressed(latestWiiData.buttonB);
        renderer.wiiButton1.setPressed(latestWiiData.button1);
        renderer.wiiButton2.setPressed(latestWiiData.button2);
        renderer.wiiButtonMin.setPressed(latestWiiData.buttonMin);
        renderer.wiiButtonHome.setPressed(latestWiiData.buttonHome);
        renderer.wiiButtonPlus.setPressed(latestWiiData.buttonPlus);
        renderer.wiiButtonUp.setPressed(latestWiiData.buttonUp);
        renderer.wiiButtonDown.setPressed(latestWiiData.buttonDown);
        renderer.wiiButtonLeft.setPressed(latestWiiData.buttonLeft);
        renderer.wiiButtonRight.setPressed(latestWiiData.buttonRight);
    }

    public void startUpdating() {
        if (updateThread == null) {
            initUpdateThread();
            updateThread.start();
        }
    }

    public void stopUpdating() {
        if (updateThread != null) {
            updateThread.interrupt();
            updateThread = null;
            latestWiiData = null;
            latestEikData = null;
        }
    }

    protected boolean isKeepUpdating() {
        return keepUpdating;
    }

    protected void setKeepUpdating(boolean result) {
        keepUpdating = result;
    }

    class ToastMessage {
        public int time;
        public String message;

        public ToastMessage(String message, int time) {
            this.message = message;
            this.time = time;
        }

    }
}
