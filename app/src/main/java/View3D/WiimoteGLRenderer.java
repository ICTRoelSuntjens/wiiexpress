package View3D;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;

import com.example.roelsuntjens.wiiexpress.R;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by roelsuntjens on 05-10-15.
 */
public class WiimoteGLRenderer implements GLSurfaceView.Renderer {
    public float angleX = 0, angleY = 0, angleZ = 0;
    public float speedX = 0, speedY = 0, speedZ = 0;
    public float z = -6.0f;
    public WiiButton wiiButtonA, wiiButtonB, wiiButton1, wiiButton2, wiiButtonMin, wiiButtonHome, wiiButtonPlus;
    public WiiButton wiiButtonCenter, wiiButtonUp, wiiButtonDown, wiiButtonLeft, wiiButtonRight;
    private boolean upsideDown = false;
    private boolean upsideX = false;
    private boolean upsideY = false;
    private Context context;
    private Wiimote wiimote;

    public WiimoteGLRenderer(Context context) {
        this.context = context;
        wiimote = new Wiimote(context);
        float depth = 0.025f;
        wiiButtonA = new WiiButton(context, 0.2f, 0.2f, depth, ImageIds.idA);
        wiiButtonB = new WiiButton(context, 0.2f, 0.3f, depth, ImageIds.idB);
        wiiButton1 = new WiiButton(context, 0.15f, 0.15f, depth, ImageIds.id1);
        wiiButton2 = new WiiButton(context, 0.15f, 0.15f, depth, ImageIds.id2);
        wiiButtonMin = new WiiButton(context, 0.1f, 0.1f, depth, ImageIds.idMin);
        wiiButtonHome = new WiiButton(context, 0.1f, 0.1f, depth, ImageIds.idHome);
        wiiButtonPlus = new WiiButton(context, 0.1f, 0.1f, depth, ImageIds.idPlus);
        float buttonLength = 0.05f, buttonWidth = 0.075f;
        wiiButtonCenter = new WiiButton(context, buttonLength, buttonLength, depth, ImageIds.idCenter);
        wiiButtonUp = new WiiButton(context, buttonLength, buttonWidth, depth, ImageIds.idUp);
        wiiButtonDown = new WiiButton(context, buttonLength, buttonWidth, depth, ImageIds.idDown);
        wiiButtonLeft = new WiiButton(context, buttonWidth, buttonLength, depth, ImageIds.idLeft);
        wiiButtonRight = new WiiButton(context, buttonWidth, buttonLength, depth, ImageIds.idRight);
    }

    public boolean isUpsideDown() {
        return upsideDown;
    }

    public void setUpsideDown(boolean result) {
        upsideDown = result;
    }

    public boolean isUpsideX() {
        return upsideX;
    }

    public void setUpsideX(boolean result) {
        upsideX = result;
    }

    public boolean isUpsideY() {
        return upsideY;
    }

    public void setUpsideY(boolean result) {
        upsideY = result;
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        gl.glClearDepthf(1.0f);
        gl.glEnable(GL10.GL_DEPTH_TEST);
        gl.glDepthFunc(GL10.GL_LEQUAL);
        gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);
        gl.glShadeModel(GL10.GL_SMOOTH);
        gl.glDisable(GL10.GL_DITHER);

        wiimote.loadTexture(gl, context);
        wiiButton1.loadTexture(gl, context);
        wiiButton2.loadTexture(gl, context);
        wiiButtonA.loadTexture(gl, context);
        wiiButtonB.loadTexture(gl, context);
        wiiButtonMin.loadTexture(gl, context);
        wiiButtonHome.loadTexture(gl, context);
        wiiButtonPlus.loadTexture(gl, context);
        wiiButtonUp.loadTexture(gl, context);
        wiiButtonDown.loadTexture(gl, context);
        wiiButtonLeft.loadTexture(gl, context);
        wiiButtonRight.loadTexture(gl, context);
        wiiButtonCenter.loadTexture(gl, context);
        gl.glEnable(GL10.GL_TEXTURE_2D);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        if (height == 0) height = 1;
        float aspect = (float) width / height;
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();
        GLU.gluPerspective(gl, 45, aspect, 0.1f, 100.f);
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();
        //....
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

        gl.glLoadIdentity();
        gl.glTranslatef(0.0f, 0, z);
        gl.glRotatef(angleX, 1.0f, 0.0f, 0.0f);
        gl.glRotatef(angleY, 0.0f, 1.0f, 0.0f);
        if (zero(angleX, 2) || zero(angleY, 2))
            gl.glRotatef(angleZ, 0.0f, 0.0f, 1.0f);
        wiimote.draw(gl);
        gl.glTranslatef(0, 0, (float) (wiimote.wiiDepth + wiiButtonA.buttonDepth * 1.3));

        int div = 8;
        boolean bap = wiiButtonA.isPressed(), bbp = wiiButtonB.isPressed(), b1p = wiiButton1.isPressed(), b2p = wiiButton2.isPressed();
        boolean bmp = wiiButtonMin.isPressed(), bhp = wiiButtonHome.isPressed(), bpp = wiiButtonPlus.isPressed();
        boolean bup = wiiButtonUp.isPressed(), bdp = wiiButtonDown.isPressed(), blp = wiiButtonLeft.isPressed(), brp = wiiButtonRight.isPressed();
        float bah = (wiiButtonA.buttonHeight / div), bbh = (wiiButtonB.buttonHeight / div), b1h = (wiiButton1.buttonHeight / div), b2h = (wiiButton2.buttonHeight / div);
        float bmh = (wiiButtonMin.buttonHeight / div), bhh = (wiiButtonHome.buttonHeight / div), bph = (wiiButtonPlus.buttonHeight / div);
        float buh = (wiiButtonPlus.buttonHeight / div), bdh = (wiiButtonDown.buttonHeight / div), blh = (wiiButtonLeft.buttonHeight / div), brh = (wiiButtonRight.buttonHeight / div);

//        drawAAndB(gl);
//        draw1And2(gl);
//        drawMinHomeAndPlus(gl);
//        drawArrows(gl);

        gl.glTranslatef(0, (float) (wiimote.wiiHeight / 2.2), (bap ? -bah : 0));
        wiiButtonA.draw(gl);
        float bheight = wiimote.wiiHeight / 8, bdepth = wiimote.wiiDepth * 2 + wiiButtonB.buttonDepth * 2;
        gl.glRotatef(180, 0, 1f, 0);
        gl.glTranslatef(0, bheight, bdepth + (bap && !bbp ? -bah : !bap && bbp ? -bbh : 0));
        wiiButtonB.draw(gl);
        gl.glTranslatef(0, -bheight, 0);
        gl.glTranslatef(0, -(float) (wiimote.wiiHeight / 1.1), -bdepth + (bbp && !b1p ? bbh : !bbp && b1p ? -b1h : 0));
        gl.glRotatef(180, 0, -1f, 0);
        wiiButton1.draw(gl);
        gl.glTranslatef(0, -(wiiButton1.buttonHeight * 3), (b1p && !b2p ? b1h : !b1p && b2p ? -b2h : 0));
        wiiButton2.draw(gl);
        float width = (float) (wiimote.wiiWidth / 1.4);
        gl.glTranslatef(-width, (float) (wiimote.wiiHeight / 1.4), (b2p && !bmp ? b2h : !b2p && bmp ? -bmh : 0));
        wiiButtonMin.draw(gl);
        gl.glTranslatef(width, 0, (bmp && !bhp ? bmh : !bmp && bhp ? -bhh : 0));
        wiiButtonHome.draw(gl);
        gl.glTranslatef(width, 0, (bhp && !bpp ? bhh : !bhp && bpp ? -bph : 0));
        wiiButtonPlus.draw(gl);

        gl.glTranslatef(-width, (float) (wiimote.wiiHeight / 1.3), (bpp ? bph : 0));
        wiiButtonCenter.draw(gl);
        float arrowLength = wiiButtonUp.buttonHeight, arrowWidth = wiiButtonUp.buttonWidth, cheight = wiiButtonCenter.buttonHeight;
        gl.glTranslatef(0, (cheight + arrowLength), 0);
        wiiButtonUp.draw(gl);
        gl.glTranslatef(0, -(arrowLength * 2 + cheight * 2), 0);
        wiiButtonDown.draw(gl);
        gl.glTranslatef(-(arrowLength + cheight), (arrowLength + cheight), 0);
        wiiButtonLeft.draw(gl);
        gl.glTranslatef((arrowLength * 2 + cheight * 2), 0, 0);
        wiiButtonRight.draw(gl);


        angleX += speedX;
        angleY += speedY;
        angleZ += speedZ;
    }

    private void drawAAndB(GL10 gl) {
    }

    private void draw1And2(GL10 gl) {

    }

    private void drawMinHomeAndPlus(GL10 gl) {

    }

    private void drawArrows(GL10 gl) {

    }

    private boolean zero(float value, int limit) {
        return (value > limit && value < -limit);
    }

    static class ImageIds {
        public static int[] idA = {
                R.drawable.buttonafront, // top
                R.drawable.side1, // left
                R.drawable.side1, // bottom
                R.drawable.side1, // right
                R.drawable.side2, // front
                R.drawable.side2, // back
                R.drawable.buttonapfront, // top
        };
        public static int[] idB = {
                R.drawable.buttonbfront, // top
                R.drawable.side1, // left
                R.drawable.side1, // bottom
                R.drawable.side1, // right
                R.drawable.side2, // front
                R.drawable.side2, // back
                R.drawable.buttonbpfront, // top
        };
        public static int[] id1 = {
                R.drawable.button1front, // top
                R.drawable.side1, // left
                R.drawable.side1, // bottom
                R.drawable.side1, // right
                R.drawable.side2, // front
                R.drawable.side2, // back
                R.drawable.button1pfront, // top
        };
        public static int[] id2 = {
                R.drawable.button2front, // top
                R.drawable.side1, // left
                R.drawable.side1, // bottom
                R.drawable.side1, // right
                R.drawable.side2, // front
                R.drawable.side2, // back
                R.drawable.button2pfront, // top
        };
        public static int[] idMin = {
                R.drawable.buttonminfront, // top
                R.drawable.side1, // left
                R.drawable.side1, // bottom
                R.drawable.side1, // right
                R.drawable.side2, // front
                R.drawable.side2, // back
                R.drawable.buttonminpfront, // top
        };
        public static int[] idHome = {
                R.drawable.buttonhomefront, // top
                R.drawable.side1, // left
                R.drawable.side1, // bottom
                R.drawable.side1, // right
                R.drawable.side2, // front
                R.drawable.side2, // back
                R.drawable.buttonhomepfront, // top
        };
        public static int[] idPlus = {
                R.drawable.buttonplusfront, // top
                R.drawable.side1, // left
                R.drawable.side1, // bottom
                R.drawable.side1, // right
                R.drawable.side2, // front
                R.drawable.side2, // back
                R.drawable.buttonpluspfront, // top
        };
        public static int[] idUp = {
                R.drawable.buttonup, // top
                R.drawable.side1, // left
                R.drawable.side1, // bottom
                R.drawable.side1, // right
                R.drawable.side2, // front
                R.drawable.side2, // back
                R.drawable.buttonupp, // top
        };
        public static int[] idDown = {
                R.drawable.buttondown, // top
                R.drawable.side1, // left
                R.drawable.side1, // bottom
                R.drawable.side1, // right
                R.drawable.side2, // front
                R.drawable.side2, // back
                R.drawable.buttondownp, // top
        };
        public static int[] idLeft = {
                R.drawable.buttonleft, // top
                R.drawable.side1, // left
                R.drawable.side1, // bottom
                R.drawable.side1, // right
                R.drawable.side2, // front
                R.drawable.side2, // back
                R.drawable.buttonleftp, // top
        };
        public static int[] idRight = {
                R.drawable.buttonright, // top
                R.drawable.side1, // left
                R.drawable.side1, // bottom
                R.drawable.side1, // right
                R.drawable.side2, // front
                R.drawable.side2, // back
                R.drawable.buttonrightp, // top
        };
        public static int[] idCenter = {
                R.drawable.buttoncenter, // top
                R.drawable.side1, // left
                R.drawable.side1, // bottom
                R.drawable.side1, // right
                R.drawable.side2, // front
                R.drawable.side2, // back
                R.drawable.buttoncenter, // top
        };
    }
}
