package View3D.Pong;

import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by roelsuntjens on 01-10-15.
 */

public abstract class PongObject {
    protected float vertices[];
    protected float width, height;
    protected FloatBuffer vertexBuffer;
    protected float base = 1.0f;
    protected float red, green, blue;
    protected float x, y, z;
    protected float scale;

    public PongObject(float vertices[], float scale, float x, float y, float z, float width, float height, float red, float green, float blue) {
        this.scale = scale;
        this.x = x;
        this.y = y;
        this.z = z;
        this.width = width;
        this.height = height;
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.vertices = vertices;
        for (int i = 0; i < vertices.length; i++) {
            vertices[i] = vertices[i] * scale;
        }
        updateVertexBuffer();
    }

    public void move(float x, float y, float z) {
        int i = 0;
        this.x = this.x + x;
        this.y = this.y + y;
        this.z = this.z + z;
        updateVertexBuffer();
    }

    public void draw(GL10 gl) {
        gl.glTranslatef(x, y, z);
        drawComponent(gl);
        gl.glTranslatef(-x, -y, -z);
    }

    protected abstract void updateVertexBuffer();

    protected abstract void drawComponent(GL10 gl);

    protected abstract float[] getX();

    public void setX(float value) {
        this.x = value;
    }

    protected abstract float[] getY();

    public void setY(float value) {
        this.y = value;
    }

    protected abstract float[] getZ();

    public void setZ(float value) {
        this.z = value;
    }

    public float X() {
        return x;
    }

    public float Y() {
        return y;
    }

    public float Z() {
        return z;
    }
}
