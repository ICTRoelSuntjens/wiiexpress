package View3D.Pong;


import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by roelsuntjens on 01-10-15.
 */
public class PongBal extends PongObject {
    private boolean dirUp = true, dirLeft = true;
    private float startX, startY, startZ;

    public PongBal(float vertices[], float scale, float x, float y, float z, float width, float height, float red, float green, float blue) {
        super(vertices, scale, x, y, z, width, height, red, green, blue);
        this.startX = x;
        this.startY = y;
        this.startZ = z;
    }

    @Override
    protected void updateVertexBuffer() {
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(4 * 3 * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        vertexBuffer = byteBuffer.asFloatBuffer();
        vertexBuffer.put(vertices);
        vertexBuffer.flip();
    }

    @Override
    public void move(float x, float y, float z) {
        super.move(x, y, z);
    }

    @Override
    protected void drawComponent(GL10 gl) {
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glColor4f(red, green, blue, 0.5f);
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
        gl.glDrawArrays(GL10.GL_TRIANGLE_FAN, 0, vertices.length / 3);
        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
    }

    @Override
    protected float[] getX() {
        return new float[0];
    }

    @Override
    protected float[] getY() {
        return new float[0];
    }

    @Override
    protected float[] getZ() {
        return new float[0];
    }

    protected boolean isDirUp() {
        return dirUp;
    }

    protected void setDirUp(boolean result) {
        this.dirUp = result;
    }

    protected boolean isDirLeft() {
        return dirLeft;
    }

    protected void setDirLeft(boolean result) {
        this.dirLeft = result;
    }

    protected void reset() {
        setX(startX);
        setY(startY);
        setZ(startZ);
    }
}
