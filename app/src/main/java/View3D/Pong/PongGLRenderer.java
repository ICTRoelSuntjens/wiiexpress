package View3D.Pong;

import android.opengl.GLU;
import android.util.Log;
import android.widget.Toast;

import com.example.roelsuntjens.wiiexpress.PlatformActivity;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import View3D.GLRenderer;

/**
 * Created by roelsuntjens on 30-09-15.
 */
public class PongGLRenderer extends GLRenderer {
    private final float barWidth = 1f, barHeight = 0.25f;
    private final float balWidth = 0.1f, balHeight = 0.1f;
    private final float screenX1 = -1.3f, screenX2 = 1.3f;
    private final float screenY1 = -2.0f, screenY2 = 1.9f;
    private PlatformActivity platformApp;
    private int canvasWidth, canvasHeight;
    private int score;
    private PongBar bar;
    private PongBal bal;
    private float barVertices[] = {
            -barWidth / 2, barHeight / 2, 0.0f,
            barWidth / 2, barHeight / 2, 0.0f,
            barWidth / 2, -barHeight / 2, 0.0f,
            -barWidth / 2, -barHeight / 2, 0.0f,
    };
    private float balVertices[] = {
            -balWidth / 2, balHeight / 2, 0.0f,
            balWidth / 2, balHeight / 2, 0.0f,
            balWidth / 2, -balHeight / 2, 0.0f,
            -balWidth / 2, -balHeight / 2, 0.0f,
    };
    private float balVertices2[] = {
            -balWidth / 2, balHeight / 2, 0.0f,
            balWidth / 2, balHeight / 2, 0.0f,
            balWidth / 2, -balHeight / 2, 0.0f,
            -balWidth / 2, -balHeight / 2, 0.0f,
    };

    public PongGLRenderer(PlatformActivity platformApp) {
        this.platformApp = platformApp;
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        this.canvasWidth = width;
        this.canvasHeight = height;
        bar = new PongBar(barVertices, 0.5f, 0, -1.8f, 0, barWidth, barHeight, 1, 0, 0);
        bal = new PongBal(balVertices, 0.5f, 0, -1f, 0, balWidth, balHeight, 1, 1, 1);
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();
        GLU.gluPerspective(gl, 45.0f, (float) width / (float) height, 0.1f, 100.0f);
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        Log.d("GLRenderer", "Surface created");
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        gl.glClearColor(0.0f, 0.0f, 0.0f, 1f);
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        gl.glLoadIdentity();
        gl.glTranslatef(0.0f, 0.0f, -5.0f);
        // draw
        bar.draw(gl);
        bal.draw(gl);
    }

    @Override
    public void update(long currentTime) {
        // TODO: Make movement smooth
        if (wiiData != null && eikData != null && bar != null) {
            updateBar();
            updateBall();
        }
    }

    private void updateBar() {
        float movex = 0, movey = 0, movez = 0.000f;
        float div = 300;
        movex = (((float) wiiData.accx - (float) eikData.eikx) / div);
//        movey = (((float) wiiData.accy - (float) eikData.eiky) / div);
        bar.move(movex, movey, movez);
        switch (barOutOfAreaX()) {
            case -1:
                bar.setX(screenX1);
                break;
            case 1:
                bar.setX(screenX2);
                break;
        }
    }

    private void updateBall() {
        float px = bal.X(), py = bal.Y(), pz = bal.Z();
        float speed = 0.02f;
        bal.move(bal.isDirLeft() ? -speed : speed, 0, 0);
        switch (ballOutOfAreaX()) {
            case -1:
                bal.setX(screenX1);
                bal.setDirLeft(false);
                break;
            case 1:
                bal.setX(screenX2);
                bal.setDirLeft(true);
                break;
        }
        if (bar.contains(bal)) {
            bal.setDirLeft(!bal.isDirLeft());
//            bal.move(bal.isDirLeft() ? speed : -speed, 0, 0);
            bal.setX(px);
//            hit();
        }
        bal.move(0, bal.isDirUp() ? speed : -speed, 0);
        switch (ballOutOfAreaY()) {
            case -1:
                bal.setY(screenY1);
                bal.setDirUp(true);
                bal.reset();
                break;
            case 1:
                bal.setY(screenY2);
                bal.setDirUp(false);
                break;
        }
        if (bar.contains(bal)) {
            bal.setDirUp(!bal.isDirUp());
            bal.setY(py);
//            bal.move(0, bal.isDirUp() ? -speed : speed, 0);
//            hit();
        }
    }

    private void hit() {
        score++;
        platformApp.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(platformApp, "Hit! Score=" + score, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private int barOutOfAreaX() {
        return bar.X() < screenX1 ? -1 : bar.X() > screenX2 ? 1 : 0;
    }

    private int ballOutOfAreaX() {
        return (bal.X() < screenX1  ? -1 : bal.X() > screenX2 ? 1 : 0);
    }

    private int ballOutOfAreaY() {
        return bal.Y() < screenY1 ? -1 : bal.Y() > screenY2 ? 1 : 0;
    }
}
