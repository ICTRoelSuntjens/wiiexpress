package View3D.Pong;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by roelsuntjens on 30-09-15.
 */
public class PongBar extends PongObject {


    public PongBar(float vertices[], float scale, float x, float y, float z, float width, float height, float red, float green, float blue) {
        super(vertices, scale, x, y, z, width, height, red, green, blue);
        updateVertexBuffer();
    }

    @Override
    protected void updateVertexBuffer() {
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(4 * 3 * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        vertexBuffer = byteBuffer.asFloatBuffer();
        vertexBuffer.put(vertices);
        vertexBuffer.flip();
    }

    @Override
    protected void drawComponent(GL10 gl) {
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glColor4f(red, green, blue, 0.5f);
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
        gl.glDrawArrays(GL10.GL_TRIANGLE_FAN, 0, vertices.length / 3);
        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
    }

    @Override
    public float[] getX() {
        return new float[]{vertices[0], vertices[3], vertices[6], vertices[9]};
    }

    @Override
    public float[] getY() {
        return new float[]{vertices[1], vertices[4], vertices[7], vertices[10]};
    }

    @Override
    public float[] getZ() {
        return new float[]{vertices[2], vertices[5], vertices[8], vertices[11]};
    }

    protected boolean contains(PongBal bal) {
        boolean part1X = bal.X() + bal.width / 4 >= this.X() - this.width / 4;
        boolean part2X = bal.X() - bal.width / 4 <= this.X() + this.width / 4;
        boolean part1Y = bal.Y() + bal.height / 4 >= this.Y() - this.height / 4;
        boolean part2Y = bal.Y() - bal.height / 4 <= this.Y() + this.height / 4;
//        if (part1X || part2X) {
//            System.out.println("Part1X{" + part1X + "} || Part2X{" + part2X + "}");
//            System.out.println("(" + (bal.X() - bal.width / 2) + " >= " + (this.X() - this.width / 2) + ") || (" + (bal.X() + bal.width / 2) + " < " + (this.X() + this.width / 2) + ")");
//        }
        if ((part1X && part2X) && (part1Y && part2Y)) {
            return true;
        }
        return false;
    }

}
