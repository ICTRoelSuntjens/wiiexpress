package View3D;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;

import com.example.roelsuntjens.wiiexpress.R;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;


/**
 * Created by roelsuntjens on 05-10-15.
 */
public class Wiimote {
    protected static float wiiWidth = 0.5f, wiiHeight = 2f, wiiDepth = 0.5f;
    private FloatBuffer vertexBuffer;
    private FloatBuffer texBuffer;
    private int numFaces = 6;
    private int[] imageFileIDs = {
            R.drawable.wiimotetop5,
            R.drawable.wiimoteleft4,
            R.drawable.wiimotebottom4,
            R.drawable.wiimoteright4,
            R.drawable.wiimotefront4,
            R.drawable.wiimoteback4,
    };
    private int[] textureIDs = new int[numFaces];
    private Bitmap[] bitmap = new Bitmap[numFaces];
    private float cubeHalfSize = 1.0f;
    private boolean exploding = true; // TODO: to remove
    private int counter = -1; // TODO: to remove

    public Wiimote(Context context) {
        ByteBuffer vbb = ByteBuffer.allocateDirect(12 * 4 * numFaces);
        vbb.order(ByteOrder.nativeOrder());
        vertexBuffer = vbb.asFloatBuffer();

        double[][] factors = {
                {wiiHeight, wiiWidth},  // top
                {wiiHeight, wiiDepth}, // left
                {wiiHeight, wiiWidth}, // bottom
                {wiiHeight, wiiDepth}, // right
                {wiiDepth, wiiWidth}, // front
                {wiiDepth, wiiWidth} // back
        };

        for (int face = 0; face < numFaces; face++) {
            bitmap[face] = BitmapFactory.decodeStream(context.getResources().openRawResource(imageFileIDs[face]));
            int imgWidth = bitmap[face].getWidth();
            int imgHeight = bitmap[face].getHeight();
            float faceHeight = (float) ((imgWidth > imgHeight ? 2f * imgHeight / imgWidth : 2f) * factors[face][0]);
            float faceWidth = (float) ((imgWidth > imgHeight ? 2f : 2f * imgWidth / imgHeight) * factors[face][1]);
            float faceLeft = -faceWidth / 2;
            float faceRight = -faceLeft;
            float faceTop = faceHeight / 2;
            float faceBottom = -faceTop;
            float[] vertices = {
                    faceLeft, faceBottom, 0.0f, // 0. left-bottom-front
                    faceRight, faceBottom, 0.0f, // 1. right-bottom-front
                    faceLeft, faceTop, 0.0f, // 2. left-top-front
                    faceRight, faceTop, 0.0f, // 3. right-top-front
            };
            vertexBuffer.put(vertices);
        }

        vertexBuffer.position(0);

        float[] texCoords = {
                0.0f, 1.0f, // A. left-bottom
                1.0f, 1.0f, // B. right-bottom
                0.0f, 0.0f, // C. left-top
                1.0f, 0.0f, // D. right-top
        };
        ByteBuffer tbb = ByteBuffer.allocateDirect(texCoords.length * 4 * numFaces);
        tbb.order(ByteOrder.nativeOrder());
        texBuffer = tbb.asFloatBuffer();
        for (int face = 0; face < numFaces; face++) {
            texBuffer.put(texCoords);
        }
        texBuffer.position(0);
    }

    public void draw(GL10 gl) {
        gl.glFrontFace(GL10.GL_CCW);
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
        gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, texBuffer);

        int texId = 0;
        drawFace(gl, 0, 0, 0, 0, wiiDepth, texId++); //top
        drawFace(gl, 270f, 0, 1f, 0, wiiWidth, texId++); // left
        drawFace(gl, 180f, 0, 1f, 0, wiiDepth, texId++); // bottom
        drawFace(gl, 90f, 0, 1f, 0, wiiWidth, texId++); // right
        drawFace(gl, 270f, 1f, 0, 0, wiiHeight, texId++); // front
        drawFace(gl, 90f, 1, 0, 0, wiiHeight, texId++); // back


        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

        if(counter != -1) {
            float inc = exploding ? 0.001f : -0.001f;
            if (++counter == 500) {
                exploding = !exploding;
                counter = 0;
            }
            wiiWidth += inc;
            wiiHeight += inc;
            wiiDepth += inc;
        }
    }

    private void drawFace(GL10 gl, float angle, float rx, float ry, float rz, float translate, int texID) {
        gl.glPushMatrix();
        gl.glRotatef(angle, rx, ry, rz);
        gl.glTranslatef(0f, 0f, translate);
        gl.glBindTexture(GL10.GL_TEXTURE_2D, textureIDs[texID]);
        gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, texID * 4, 4);
        gl.glPopMatrix();
    }

    public void loadTexture(GL10 gl, Context context) {
        gl.glGenTextures(6, textureIDs, 0);

        InputStream istream;
        for (int face = 0; face < numFaces; face++) {
            gl.glBindTexture(GL10.GL_TEXTURE_2D, textureIDs[face]);
            gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST);
            gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
            istream = context.getResources().openRawResource(imageFileIDs[face]);
            try {
                bitmap[face] = BitmapFactory.decodeStream(istream);
            } finally {
                try {
                    istream.close();
                } catch (IOException e) {
                }
            }
            GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap[face], 0);
        }
    }
}