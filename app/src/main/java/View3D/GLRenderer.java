package View3D;

import android.opengl.GLSurfaceView;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import model.EikData;
import model.WiiData;

/**
 * Created by roelsuntjens on 30-09-15.
 */
public abstract class GLRenderer implements GLSurfaceView.Renderer {

    protected final float screenX1 = -1.3f, screenX2 = 1.3f;
    protected final float screenY1 = -2.0f, screenY2 = 1.9f;
    protected WiiData wiiData;
    protected EikData eikData;
    @Override
    public abstract void onSurfaceChanged(GL10 gl, int width, int height);


    @Override
    public abstract void onSurfaceCreated(GL10 gl, EGLConfig config);

    @Override
    public abstract void onDrawFrame(GL10 gl);

    public abstract void update(long currentTime);

    public void setEikData(EikData data) {
        this.eikData = data;
    }

    public void setWiiData(WiiData data) {
        this.wiiData = data;
    }
}
