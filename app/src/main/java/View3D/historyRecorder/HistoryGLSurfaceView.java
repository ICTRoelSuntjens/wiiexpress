package View3D.historyRecorder;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.widget.Toast;

import com.example.roelsuntjens.wiiexpress.HistoryActivity;

import java.util.ArrayList;
import java.util.List;

import API.service;
import View3D.WiimoteGLRenderer;
import model.EikData;
import model.EikDataResponse;
import model.WiiData;
import model.WiiDataResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by roelsuntjens on 06/11/15.
 */
public class HistoryGLSurfaceView extends GLSurfaceView {
    private static final String NOSERVER = "Could not connect to the server";
    private List<ToastMessage> toastMessages = new ArrayList<ToastMessage>();
    private WiimoteGLRenderer renderer;
    private Thread updateThread = null;
    private WiiData latestWiiData = null;
    private EikData latestEikData = null;
    private long pastEikTime = 0, eikDelay = 10 * 1000;
    private long pastWiiTime = 0, wiiDelay = 15 * 1000;
    private boolean keepUpdating = true;
    private int keepUpdatingCounter = 0, playRecordingIndex = 0;
    private HistoryActivity app;

    private List<WiiData> recordedWiiData = new ArrayList<>();
    private boolean startedRecording = false, endedRecording = false;

    public HistoryGLSurfaceView(Context context, HistoryActivity app) {
        super(context);
        this.app = app;
        renderer = new WiimoteGLRenderer(context);
        this.setRenderer(renderer);
        this.requestFocus();
        this.setFocusableInTouchMode(true);
        init();
        startUpdating();
    }

    private void init() {
    }

    private void initUpdateThread() {
        updateThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (true) {
                        long currentTime = System.currentTimeMillis();
                        if ((keepUpdating) || (pastWiiTime + wiiDelay < currentTime && !keepUpdating)) {
                            if (pastWiiTime + wiiDelay < currentTime && !keepUpdating) {
                                showToastMessages();
                            }
                            pastWiiTime = currentTime;
//                            System.out.println("Check for wiidata");
                            update(currentTime);
                        }
                        Thread.sleep(1000 / 10);
                    }
                } catch (InterruptedException e) {
                    System.out.println("WiimoteSurface -> updatethread interrupted");
                }
            }
        });
    }

    private void showToastMessages() {
        for (int i = toastMessages.size() - 1; i > 0; i--) {
            app.showToast(toastMessages.get(i).message, toastMessages.get(i).time);
            toastMessages.remove(i);
        }
    }

    private void update(long currentTime) {
        if ((startedRecording && !endedRecording) || (!startedRecording && !endedRecording)) {
            if (pastEikTime + eikDelay < currentTime) {
                pastEikTime = currentTime;
                service.getInstace().getLatestEikData(new Callback<EikDataResponse>() {
                    @Override
                    public void success(EikDataResponse eikDataResponse, Response response) {
                        if (eikDataResponse.results.size() > 0) {
                            latestEikData = eikDataResponse.results.get(0);
                        } else {
                            System.out.println("Eikdata not found");
                            toastMessages.add(new ToastMessage("Eik your wiimode", Toast.LENGTH_LONG));
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        latestEikData = null;
                        toastMessages.add(new ToastMessage(NOSERVER, Toast.LENGTH_LONG));
                        System.out.println("Eikdata failed");
                    }
                });
            }
            service.getInstace().getLatestData(new Callback<WiiDataResponse>() {
                @Override
                public void success(WiiDataResponse wiiDataResponse, Response response) {
                    if (wiiDataResponse.results != null) {
                        if (wiiDataResponse.results.size() > 0) {
                            latestWiiData = wiiDataResponse.results.get(0);
                            if (latestEikData != null && latestWiiData != null) {
                                updateView(latestWiiData);
                            }
                            if (wiiDataResponse.results.get(0).buttonA == 1 && !startedRecording & !endedRecording) {
                                recordedWiiData.clear();
                                startedRecording = true;
                                System.out.println("Started recording");
                            } else if (wiiDataResponse.results.get(0).buttonA == 1 && startedRecording && !endedRecording) {
                                if (recordedWiiData.size() > 0) {
                                    if (recordedWiiData.get(recordedWiiData.size() - 1).buttonA != 1) {
                                        endedRecording = true;
                                        System.out.println("Ended recording");
                                    }
                                }
                            }
                            if (startedRecording) {
                                System.out.println("Recording...");
                                recordedWiiData.add(wiiDataResponse.results.get(0));
                            }
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("Failed!");
                }
            });
        } else {
            if (recordedWiiData.size() > 0 && startedRecording && endedRecording)
                playRecording();
            else {
                System.out.println("Don't know");
            }
        }
    }

    private void playRecording() {
        System.out.println("Play Recording");
        playRecordingIndex = playRecordingIndex < recordedWiiData.size() - 2 ? playRecordingIndex + 1 : 0;
        updateView(recordedWiiData.get(playRecordingIndex));
    }

    private void updateView(WiiData data) {
        float anglex = (float) (((double) 90 / (double) 24) * (latestEikData.eiky - data.accy));
        float angley = (float) (((double) 90 / (double) 24) * (latestEikData.eikx - data.accx));
        float anglez = (latestEikData.eikz - data.accz);
        renderer.setUpsideDown(anglez > 24);
        renderer.setUpsideX(renderer.isUpsideDown() && ((anglex > 85 || anglex < -85) || renderer.isUpsideX()));
        renderer.setUpsideY(renderer.isUpsideDown() && ((angley > 85 || angley < -85) || renderer.isUpsideY()));
        if (renderer.isUpsideX())
            anglex = 90 + (90 - anglex);
        if (renderer.isUpsideY())
            angley = 90 + (90 - angley);
        renderer.angleX = anglex;
        renderer.angleY = -angley;
        renderer.angleZ = anglez;
        updateWiiButtons(data);
    }

    private void updateWiiButtons(WiiData data) {
        renderer.wiiButtonA.setPressed(data.buttonA);
        renderer.wiiButtonB.setPressed(data.buttonB);
        renderer.wiiButton1.setPressed(data.button1);
        renderer.wiiButton2.setPressed(data.button2);
        renderer.wiiButtonMin.setPressed(data.buttonMin);
        renderer.wiiButtonHome.setPressed(data.buttonHome);
        renderer.wiiButtonPlus.setPressed(data.buttonPlus);
        renderer.wiiButtonUp.setPressed(data.buttonUp);
        renderer.wiiButtonDown.setPressed(data.buttonDown);
        renderer.wiiButtonLeft.setPressed(data.buttonLeft);
        renderer.wiiButtonRight.setPressed(data.buttonRight);
    }

    public void startUpdating() {
        if (updateThread == null) {
            initUpdateThread();
            updateThread.start();
        }
    }

    public void stopUpdating() {
        if (updateThread != null) {
            updateThread.interrupt();
            updateThread = null;
            latestWiiData = null;
            latestEikData = null;
        }
    }

    protected boolean isKeepUpdating() {
        return keepUpdating;
    }

    protected void setKeepUpdating(boolean result) {
        keepUpdating = result;
    }

    class ToastMessage {
        public int time;
        public String message;

        public ToastMessage(String message, int time) {
            this.message = message;
            this.time = time;
        }

    }
}
