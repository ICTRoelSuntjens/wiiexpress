package View3D;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by roelsuntjens on 08-10-15.
 */
public class WiiButton {
    protected float buttonWidth, buttonHeight, buttonDepth;
    private FloatBuffer vertexBuffer;
    private FloatBuffer texBuffer;
    private int numFaces = 6;
    private int[] imageFileIDs;
    private int[] textureIDs = new int[numFaces + 1];
    private Bitmap[] bitmap = new Bitmap[numFaces + 1];
    private boolean pressed = false;

    public WiiButton(Context context, float width, float height, float depth, int[] imageIDs) {
        if (imageIDs.length < 6) {
            throw new IndexOutOfBoundsException("Not enough image Id's");
        } else if (imageIDs.length < 7) {
            throw new NullPointerException("No pressed state id found");
        }
        this.buttonWidth = width;
        this.buttonHeight = height;
        this.buttonDepth = depth;
        this.imageFileIDs = imageIDs;
        ByteBuffer vbb = ByteBuffer.allocateDirect(12 * 4 * numFaces);
        vbb.order(ByteOrder.nativeOrder());
        vertexBuffer = vbb.asFloatBuffer();

        double[][] factors = {
                {height, width},  // top
                {height, depth}, // left
                {height, width}, // bottom
                {height, depth}, // right
                {depth, width}, // front
                {depth, width} // back
        };

        for (int face = 0; face < numFaces; face++) {
            bitmap[face] = BitmapFactory.decodeStream(context.getResources().openRawResource(imageFileIDs[face]));
            int imgWidth = bitmap[face].getWidth();
            int imgHeight = bitmap[face].getHeight();
            float faceHeight = (float) ((imgWidth > imgHeight ? 2f * imgHeight / imgWidth : 2f) * factors[face][0]);
            float faceWidth = (float) ((imgWidth > imgHeight ? 2f : 2f * imgWidth / imgHeight) * factors[face][1]);
            float faceLeft = -faceWidth / 2;
            float faceRight = -faceLeft;
            float faceTop = faceHeight / 2;
            float faceBottom = -faceTop;
            float[] vertices = {
                    faceLeft, faceBottom, 0.0f, // 0. left-bottom-front
                    faceRight, faceBottom, 0.0f, // 1. right-bottom-front
                    faceLeft, faceTop, 0.0f, // 2. left-top-front
                    faceRight, faceTop, 0.0f, // 3. right-top-front
            };
            vertexBuffer.put(vertices);
        }

        vertexBuffer.position(0);

        float[] texCoords = {
                0.0f, 1.0f, // A. left-bottom
                1.0f, 1.0f, // B. right-bottom
                0.0f, 0.0f, // C. left-top
                1.0f, 0.0f, // D. right-top
        };
        ByteBuffer tbb = ByteBuffer.allocateDirect(texCoords.length * 4 * numFaces);
        tbb.order(ByteOrder.nativeOrder());
        texBuffer = tbb.asFloatBuffer();
        for (int face = 0; face < numFaces; face++) {
            texBuffer.put(texCoords);
        }
        texBuffer.position(0);
    }

    public void draw(GL10 gl) {
        gl.glFrontFace(GL10.GL_CCW);
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
        gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, texBuffer);

        int texId = 0;
        drawFace(gl, 0, 0, 0, 0, buttonDepth, (isPressed() ? /*textureIDs.length - 1 */ 6 : texId), texId++); //top
        drawFace(gl, 270f, 0, 1f, 0, buttonWidth, texId, texId++); // left
        drawFace(gl, 180f, 0, 1f, 0, buttonDepth, texId, texId++); // bottom
        drawFace(gl, 90f, 0, 1f, 0, buttonWidth, texId, texId++); // right
        drawFace(gl, 270f, 1f, 0, 0, buttonHeight, texId, texId++); // front
        drawFace(gl, 90f, 1f, 0, 0, buttonHeight, texId, texId++); // back


        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
    }

    private void drawFace(GL10 gl, float angle, float rx, float ry, float rz, float translate, int textureId, int texID) {
        gl.glPushMatrix();
        gl.glRotatef(angle, rx, ry, rz);
        gl.glTranslatef(0f, 0f, translate);
        gl.glBindTexture(GL10.GL_TEXTURE_2D, textureIDs[textureId]);
        gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, texID * 4, 4);
        gl.glPopMatrix();
    }

    public void loadTexture(GL10 gl, Context context) {
        gl.glGenTextures(7, textureIDs, 0);

        InputStream istream;
        for (int face = 0; face < numFaces + 1; face++) {
            gl.glBindTexture(GL10.GL_TEXTURE_2D, textureIDs[face]);
            gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST);
            gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
            istream = context.getResources().openRawResource(imageFileIDs[face]);
            try {
                bitmap[face] = BitmapFactory.decodeStream(istream);
            } finally {
                try {
                    istream.close();
                } catch (IOException e) {
                }
            }
            GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap[face], 0);
        }
    }

    public boolean isPressed() {
        return pressed;
    }

    public void setPressed(boolean result) {
        this.pressed = result;
    }

    public void setPressed(int result) {
        this.pressed = result == 1 ? true : false;
    }

    public float getWidth() {
        return buttonWidth * 2;
    }

    public float getHeight() {
        return buttonHeight * 2;
    }

    public float getDepth() {
        return buttonDepth * 2;
    }
}
