package Singleton;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.util.List;

import API.Querries;
import API.service;
import model.Command;
import model.UserData;
import model.UserDataResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by roelsuntjens on 08-09-15.
 */
public class DataStorage {
    private static DataStorage ourInstance = new DataStorage(); // singleton

    private static String PHEFS_NAME = "Settings";
    private static String LOGIN_CB_USERNAME = "login_cb_username", LOGIN_CB_PASSWORD = "login_cb_password";
    private static String LOGIN_TV_USERNAME = "login_tv_username", LOGIN_TV_PASSWORD = "login_tv_password";

    private static SharedPreferences prefts;
    private AboutData aboutData;

    private DataStorage() {
        SystemPrint(this.getClass(), "Constructor", "Data Storage Singleton Created");
        aboutData = new AboutData();
    }

    public static DataStorage getInstance() {
        return ourInstance;
    }

    public static void init(AppCompatActivity app) {
        SystemPrint(DataStorage.class, "init", "init....");
        if (prefts == null)
            prefts = app.getApplicationContext().getSharedPreferences(PHEFS_NAME, 0);
//        LoginData.Init(app, null, null);
        UserClass.Init();
    }

    public static void SystemPrint(Class obj, String method, String line) {
        System.out.println(obj.toString() + " -> " + method + " -> " + line);
    }

    public AboutData getAboutData() {
        return aboutData;
    }

    public static class LoginData {
        private static String LOGIN_LOGGEDIN_USERNAME = null;

        public static void Init(final Context context, final Command succes, final Command failed) {
            final String[][] missingUsers = {
                    // {firstname, lastname, username, password, editable}
                    {"ad", "min", "admin", "admin", "99", "false"},
                    {"roel", "suntjens", "roel", "suntjens", "20", "false"},
                    {"roderik", "adang", "roderik", "adang", "10", "true"},
                    {"arno", "klarenbeek", "arno", "klarenbeek", "10", "true"},
                    {"ebru", "gungor", "ebru", "gungor", "10", "true"}
            };
            boolean changed = false;
            for (int i = 0; i < missingUsers.length; i++) {
                changed = true;
                final int index = i;
                service.getInstace().userByQuery(Querries.getUserExists(missingUsers[i][2]), new Callback<UserDataResponse>() {
                    @Override
                    public void success(UserDataResponse userDataResponse, Response response) {
                        if (userDataResponse.results.size() == 0) {
                            UserData user = new UserData(missingUsers[index][0], missingUsers[index][1], missingUsers[index][2], missingUsers[index][3], Integer.parseInt(missingUsers[index][4]));
                            user.editable = Boolean.parseBoolean(missingUsers[index][5]);
                            service.getInstace().addUser(user, new Callback<UserDataResponse>() {
                                @Override
                                public void success(UserDataResponse userDataResponse, Response response) {
                                    DataStorage.SystemPrint(this.getClass(), "LoginData Init", "Missing user added [" + missingUsers[index][0] + "]");
                                    if (index == missingUsers.length - 1)
                                        if (succes != null)
                                            succes.execute();
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    DataStorage.SystemPrint(this.getClass(), "LoginData Init", "Failure!! Reason: " + error.getMessage());
                                    Toast.makeText(context, "Missing users['" + missingUsers[index][0] + "'] could not be added", Toast.LENGTH_SHORT).show();
                                    if (index == missingUsers.length - 1)
                                        if (failed != null)
                                            failed.execute();
                                }
                            });
                        } else {
                            if (index == missingUsers.length - 1)
                                if (succes != null)
                                    succes.execute();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(context, "Could not reach the server [DataStorage init, Login]", Toast.LENGTH_SHORT).show();
                        if (index == missingUsers.length - 1)
                            if (failed != null)
                                failed.execute();
                    }
                });
                if (!changed)
                    if (succes != null)
                        succes.execute();
            }
        }

        public static void SaveUserData(String user, String pass, boolean userSave, boolean passSave) {
            SharedPreferences.Editor edit = prefts.edit();
            String line = "";
            if (userSave) {
                edit.putString(LOGIN_TV_USERNAME, user);
                edit.putBoolean(LOGIN_CB_USERNAME, userSave);
                line += "[username]";
            } else {
                edit.remove(LOGIN_TV_USERNAME);
                edit.remove(LOGIN_CB_USERNAME);
            }
            if (passSave) {
                edit.putString(LOGIN_TV_PASSWORD, pass);
                edit.putBoolean(LOGIN_CB_PASSWORD, passSave);
                line += "[password]";
            } else {
                edit.remove(LOGIN_TV_PASSWORD);
                edit.remove(LOGIN_CB_PASSWORD);
            }
            edit.commit();
            SystemPrint(DataStorage.class, "SaveUserData", "Saved: " + line);
        }

        public static void SaveUsernameCheckbox(boolean result) {
            prefts.edit().putBoolean(LOGIN_CB_USERNAME, result);
            prefts.edit().commit();
        }

        public static void SavePasswordCheckbox(boolean result) {
            prefts.edit().putBoolean(LOGIN_CB_PASSWORD, result);
            prefts.edit().commit();
        }

        public static boolean contains(String key) {
            return prefts.contains(key);
        }

        public static boolean UsernameCheckboxIsChecked() {
            if (prefts.contains(LOGIN_CB_USERNAME))
                return prefts.getBoolean(LOGIN_CB_USERNAME, false);
            return false;
        }

        public static boolean PasswordCheckboxIsChecked() {
            if (prefts.contains(LOGIN_CB_PASSWORD))
                return prefts.getBoolean(LOGIN_CB_PASSWORD, false);
            return false;
        }

        public static String GetLoginUsername() {
            SystemPrint(DataStorage.class, "GetLoginUsername", "[" + prefts.getString(LOGIN_TV_USERNAME, "") + "]");
            return prefts.getString(LOGIN_TV_USERNAME, "");
        }

        public static String GetLoginPassword() {
            SystemPrint(DataStorage.class, "GetLoginPassword", "[" + prefts.getString(LOGIN_TV_PASSWORD, "") + "]");
            return prefts.getString(LOGIN_TV_PASSWORD, "");
        }
    }

    public static class UserClass {
        private static String USERSTORAGE = "Users";
        private static UserData currentUser = null;

        public static void Init() {
        }

        public static void SaveUsers(List<UserData> users) {
            for (UserData user : users) {
                service.getInstace().addUser(user, new Callback<UserDataResponse>() {
                    @Override
                    public void success(UserDataResponse userDataResponse, Response response) {
                        SystemPrint(this.getClass(), "Save Users", "User was added");
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        SystemPrint(this.getClass(), "Save Users", "User could not be added! Reason: " + error.getMessage());
                    }
                });
            }
        }

        public static void AddUserToDatabase(final String firstname, final String lastname, final String username, final String password) {
            AddUserToDatabase(new UserData(firstname, lastname, username, password, 0));
        }

        public static void AddUserToDatabase(final UserData user) {
            service.getInstace().addUser(user, new Callback<UserDataResponse>() {
                @Override
                public void success(UserDataResponse userDataResponse, Response response) {
                    SystemPrint(this.getClass(), "AddUserToDatabase", "User succesfully added to the database");
                }

                @Override
                public void failure(RetrofitError error) {
                    SystemPrint(this.getClass(), "AddUserToDatabase", "User [" + user.username + "]could not get added to the database! Reason: " + error.getMessage());
                }
            });
        }

        public static UserData getCurrentUser() {
            return currentUser;
        }

        public static void setCurrentUser(UserData user) {
            currentUser = user;
        }
    }

    public class AboutData {
        public String header1 = null;
        public String header2 = null;
        public String header3 = null;
        public String header4 = null;
        public String header5 = null;
        public String header6 = null;

        public boolean isLoaded() {
            boolean loaded = true;
            loaded &= header1 != null;
            loaded &= header2 != null;
            loaded &= header3 != null;
            loaded &= header4 != null;
            loaded &= header5 != null;
            loaded &= header6 != null;
            return loaded;
        }
    }
}
