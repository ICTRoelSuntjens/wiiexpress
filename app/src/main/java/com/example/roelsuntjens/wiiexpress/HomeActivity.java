package com.example.roelsuntjens.wiiexpress;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import Singleton.DataStorage;
import model.UserData;

public class HomeActivity extends AppCompatActivity {
    private Button liveFeedButton, accesButton, aboutButton, logoutButton, pongButton, shipButton, wiimote3DButton, historyButton;
    private UserData currentUser = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        System.out.println("On create Home");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
//        startActivity(new Intent(HomeActivity.this, LiveFeedActivity.class));
//        startActivity(new Intent(HomeActivity.this, PlatformActivity.class));
//        startActivity(new Intent(HomeActivity.this, Wiimote3DActivity.class));
        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
//        DataStorage.UserClass.setCurrentUser(null);
//        navigateToLogin();
        super.onBackPressed();
    }


    private void init() {
        DataStorage.SystemPrint(this.getClass(), "Init", "Init Home Activity");
        currentUser = DataStorage.UserClass.getCurrentUser();
        if (currentUser == null) {
            navigateToLogin();
            finish();
        } else {
            initButtons();
        }
    }

    private void initButtons() {
        System.out.println("> Init Buttons<");
        if (currentUser != null) {
            accesButton = (Button) findViewById(R.id.accesButton);
            liveFeedButton = (Button) findViewById(R.id.liveFeedButton);
            pongButton = (Button) findViewById(R.id.pongButton);
            shipButton = (Button) findViewById(R.id.shipButton);
            wiimote3DButton = (Button) findViewById(R.id.wiimote3DButton);
            historyButton = (Button) findViewById(R.id.historyButton);
            aboutButton = (Button) findViewById(R.id.aboutButton);
            logoutButton = (Button) findViewById(R.id.logoutButton);

            accesButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    navigateToAcces();
                }
            });
            aboutButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    navigateToAbout();
                }
            });
            liveFeedButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    navigateToLiveFeed();
                }
            });
            pongButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    navigateToPlatformView(PlatformActivity.Pong);
                }
            });
            shipButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    navigateToPlatformView(PlatformActivity.ShootingShip);
                }
            });
            wiimote3DButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    navigateToWii3DMode();
                }
            });
            historyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    navigateToRecorder();
                }
            });
            logoutButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DataStorage.UserClass.setCurrentUser(null);
//                    navigateToLogin();
                    init();
                }
            });
            if (currentUser.acces >= 10) {
                accesButton.setEnabled(true);
                accesButton.setVisibility(View.VISIBLE);
            }
            if (currentUser.acces >= 1) {
                liveFeedButton.setEnabled(true);
                pongButton.setEnabled(true);
                shipButton.setEnabled(true);
                wiimote3DButton.setEnabled(true);
                historyButton.setEnabled(true);
            }
            aboutButton.setEnabled(true);
            logoutButton.setEnabled(true);
        } else {
            Toast.makeText(HomeActivity.this, "Login did go wrong", Toast.LENGTH_SHORT);
            navigateToLogin();
        }
    }

    private void navigateToAcces() {
        startActivity(new Intent(HomeActivity.this, AccesActivity.class));
    }

    private void navigateToLiveFeed() {
        startActivity(new Intent(HomeActivity.this, LiveFeedActivity.class));
    }

    private void navigateToAbout() {
        startActivity(new Intent(HomeActivity.this, AboutActivity.class));
    }

    private void navigateToLogin() {
        startActivity(new Intent(HomeActivity.this, LoginActivity.class));
    }

    private void navigateToPlatformView(int platform) {
        Intent intent = new Intent(HomeActivity.this, PlatformActivity.class);
        intent.putExtra("Platform", platform);
        startActivity(intent);
    }

    private void navigateToWii3DMode() {
        startActivity(new Intent(HomeActivity.this, Wiimote3DActivity.class));
    }

    private void navigateToRecorder() {
        startActivity(new Intent(HomeActivity.this, HistoryActivity.class));
    }
}
