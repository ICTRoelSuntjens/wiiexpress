package com.example.roelsuntjens.wiiexpress;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import API.service;
import Singleton.DataStorage;
import model.AboutData;
import model.AboutDataResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AboutActivity extends AppCompatActivity {
    private TextView header1, header2, header3, header4, header5, header6, feedback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_about, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(AboutActivity.this, HomeActivity.class));
    }

    private void init() {
        initTextViews();
    }

    private void initTextViews() {
        header1 = (TextView) findViewById(R.id.about_header1);
        header2 = (TextView) findViewById(R.id.about_header2);
        header3 = (TextView) findViewById(R.id.about_header3);
        header4 = (TextView) findViewById(R.id.about_header4);
        header5 = (TextView) findViewById(R.id.about_header5);
        header6 = (TextView) findViewById(R.id.about_header6);
        feedback = (TextView) findViewById(R.id.about_feedback_txt);
        setupHeaders();
    }

    private void setupHeaders() {
        if (DataStorage.getInstance().getAboutData().isLoaded()) {
            DataStorage.AboutData data = DataStorage.getInstance().getAboutData();
            header1.setText(data.header1);
            header2.setText(data.header2);
            header3.setText(data.header3);
            header4.setText(data.header4);
            header5.setText(data.header5);
            header6.setText(data.header6);
        } else {
            service.getInstace().getAllColumns(new Callback<AboutDataResponse>() {
                @Override
                public void success(AboutDataResponse aboutDataResponse, Response response) {
                    if (aboutDataResponse.results.size() > 0) {
//                    feedback.setText("Data received");
                        DataStorage.AboutData aboutData = DataStorage.getInstance().getAboutData();
                        for (AboutData data : aboutDataResponse.results) {
                            switch (data.columnNr) {
                                case 1:
                                    header1.setText(data.context);
                                    aboutData.header1 = data.context;
                                    break;
                                case 2:
                                    header2.setText(data.context);
                                    aboutData.header2 = data.context;
                                    break;
                                case 3:
                                    header3.setText(data.context);
                                    aboutData.header3 = data.context;
                                    break;
                                case 4:
                                    header4.setText(data.context);
                                    aboutData.header4 = data.context;
                                    break;
                                case 5:
                                    header5.setText(data.context);
                                    aboutData.header5 = data.context;
                                    break;
                                case 6:
                                    header6.setText(data.context);
                                    aboutData.header6 = data.context;
                                    break;
                            }
                        }
                    } else {
                        feedback.setText("No data received");
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    feedback.setText("Could not reache server");
                }
            });
        }
    }

}
