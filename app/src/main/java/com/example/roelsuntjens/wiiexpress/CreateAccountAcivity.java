package com.example.roelsuntjens.wiiexpress;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import API.Querries;
import API.service;
import Singleton.DataStorage;
import model.UserData;
import model.UserDataResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CreateAccountAcivity extends AppCompatActivity {
    private String allreadyExists = "Account allready exists";
    private Button createButton;
    private TextView createFirstnameTxt, createLastname_Txt, createUsername_Txt, createPassword_Txt;
    private EditText createFirstnameEdit, createLastname_Edit, createUsername_Edit, createPassword_Edit;
    private TextView createFeedback_Txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account_acivity);
        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_account_acivity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void init() {
        DataStorage.SystemPrint(this.getClass(), "init", "Init create account activity");
        initButtons();
        initFields();
    }

    private void initButtons() {
        createButton = (Button) findViewById(R.id.createButton);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String firstname = createFirstnameEdit.getText().toString();
                final String lastname = createLastname_Edit.getText().toString();
                final String username = createUsername_Edit.getText().toString();
                final String password = createPassword_Edit.getText().toString();
                if (!fieldsFilled(firstname, lastname, username, password))
                    return;
                service.getInstace().userByQuery(Querries.getUserExists(username), new Callback<UserDataResponse>() {
                    @Override
                    public void success(UserDataResponse userDataResponse, Response response) {
                        DataStorage.SystemPrint(this.getClass(), "initButtons", "InitTry again");
                        if (userDataResponse.results.size() == 0) {
                            DataStorage.UserClass.AddUserToDatabase(firstname, lastname, username, password);
                            DataStorage.LoginData.SaveUserData(username, password, true, true);
                            gotoLoginScreen();
                        } else {
                            createFeedback_Txt.setText(allreadyExists);
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        createFeedback_Txt.setText("Server is nog responding");
                        System.out.println("Error: " + error.getMessage());
                    }
                });
            }
        });
    }

    private boolean fieldsFilled(String firstname, String lastname, String username, String password) {
        if (firstname == null || firstname.equals("")) {
            createFirstnameTxt.setBackgroundColor(Color.RED);
            return false;
        } else {
            createFirstnameTxt.setBackgroundColor(Color.BLACK);
        }
        if (lastname == null || lastname.equals("")) {
            createLastname_Txt.setBackgroundColor(Color.RED);
            return false;
        } else {
            createLastname_Txt.setBackgroundColor(Color.BLACK);
        }
        if (username == null || username.equals("")) {
            createUsername_Txt.setBackgroundColor(Color.RED);
            return false;
        } else {
            createUsername_Txt.setBackgroundColor(Color.BLACK);
        }
        if (password == null || password.equals("")) {
            createPassword_Txt.setBackgroundColor(Color.RED);
            return false;
        } else {
            createPassword_Txt.setBackgroundColor(Color.BLACK);
        }
        return true;
    }

    private void initFields() {
        createFirstnameTxt = (TextView) findViewById(R.id.create_firstname_txt);
        createFirstnameEdit = (EditText) findViewById(R.id.create_firstname_edit);
        createLastname_Txt = (TextView) findViewById(R.id.create_lastname_txt);
        createLastname_Edit = (EditText) findViewById(R.id.create_lastname_edit);
        createUsername_Txt = (TextView) findViewById(R.id.create_username_txt);
        createUsername_Edit = (EditText) findViewById(R.id.create_username_edit);
        createPassword_Txt = (TextView) findViewById(R.id.create_password_txt);
        createPassword_Edit = (EditText) findViewById(R.id.create_password_edit);
        createFeedback_Txt = (TextView) findViewById(R.id.registerFeedbackField);
    }

    private void gotoLoginScreen() {
        startActivity(new Intent(CreateAccountAcivity.this, LoginActivity.class));
    }
}
