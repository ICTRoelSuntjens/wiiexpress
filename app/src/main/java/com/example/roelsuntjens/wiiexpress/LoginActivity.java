package com.example.roelsuntjens.wiiexpress;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import API.Querries;
import API.service;
import Singleton.DataStorage;
import model.Command;
import model.PersonResponse;
import model.UserData;
import model.UserDataResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LoginActivity extends AppCompatActivity {
    private String wrongUsername = "Username not exists", wrongPassword = "Wrong password";
    private Button loginButton, createButton;
    private EditText loginName, loginPassword;
    private TextView loginFeedbackField;
    private CheckBox cbRememberName, cbRememberPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        DataStorage.UserClass.setCurrentUser(new UserData("ad", "min", "admin", "admin", 99));
        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
        init();
//        test();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        DataStorage.SystemPrint(this.getClass(), "onCreateOptionsMenu", "Menu: " + menu.toString());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        DataStorage.SystemPrint(this.getClass(), "OnOptionsItemSelected", "selected id: " + id);

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public void onBackPressed() {
//    }

    private void init() {
        DataStorage.SystemPrint(this.getClass(), "Init", "Login Activity -> Init");
        DataStorage.init(this);
        initCheckboxes();
        initFields();
        initButtons();
    }

    private void initCheckboxes() {
        cbRememberName = (CheckBox) findViewById(R.id.rememberNameCB);
        cbRememberPass = (CheckBox) findViewById(R.id.rememberPassCB);
        loadLastCheckboxStates();
    }

    private void initFields() {
        loginName = (EditText) findViewById(R.id.loginEditName);
        loginPassword = (EditText) findViewById(R.id.loginEditPassword);
        loginFeedbackField = (TextView) findViewById(R.id.loginFeedbackField);
        loadFieldStatus();
    }

    private void initButtons() {
        loginButton = (Button) findViewById(R.id.loginButton);
        createButton = (Button) findViewById(R.id.createButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataStorage.LoginData.Init(LoginActivity.this, new Command() {
                    @Override
                    public void execute() {
                        login();
                    }
                }, null);
            }
        });
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                create();
            }
        });
    }

    private void login() {
        final String username = "" + loginName.getText();
        final String password = "" + loginPassword.getText();
        remeberUsername();
        service.getInstace().userByQuery(Querries.getUserExists(username), new Callback<UserDataResponse>() {
            @Override
            public void success(UserDataResponse userDataResponse, Response response) {
                if (userDataResponse.results.size() > 0) {
                    if (userDataResponse.results.get(0).username.equals(username)) {
                        loginFeedbackField.setText("Login username succesfull");
                        service.getInstace().userByQuery(Querries.getPasswordCorrect(username, password), new Callback<UserDataResponse>() {
                            @Override
                            public void success(UserDataResponse userDataResponse, Response response) {
                                if (userDataResponse.results.size() > 0) {
                                    if (userDataResponse.results.get(0).password.equals(password)) {
                                        loginFeedbackField.setText("Login password succesfull");
                                        DataStorage.UserClass.setCurrentUser(userDataResponse.results.get(0));
                                        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                                        finish();
                                    } else {
                                        loginFeedbackField.setText("(2)" + wrongPassword);
                                    }
                                } else {
                                    loginFeedbackField.setText("(1)" + wrongPassword);
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                loginFeedbackField.setText("Server error[Login pass]");
                            }
                        });
                    } else {
                        loginFeedbackField.setText("(2)" + wrongUsername);
                    }
                } else {
                    loginFeedbackField.setText("(1)" + wrongUsername);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                loginFeedbackField.setText("Server error[Login user]");
                loadFieldStatus();
            }
        });
    }

    private void create() {
        startActivity(new Intent(LoginActivity.this, CreateAccountAcivity.class));
    }

    /*
    * when remember is true, the username will be saved.
    * if remember is false, the previous saved username will be lost as well.
    * the state of the checkbox will be saved aswell.
     */
    private boolean remeberUsername() {
        boolean rememberUser = cbRememberName.isChecked();
        boolean rememberPass = cbRememberPass.isChecked();
        DataStorage.LoginData.SaveUserData(loginName.getText().toString(), loginPassword.getText().toString(), rememberUser, rememberPass);
        return true;
    }

    private boolean loadLastCheckboxStates() {
        cbRememberName.setChecked(DataStorage.LoginData.UsernameCheckboxIsChecked());
        cbRememberPass.setChecked(DataStorage.LoginData.PasswordCheckboxIsChecked() && cbRememberName.isChecked());
        return true;
    }

    private boolean loadFieldStatus() {
        DataStorage.SystemPrint(this.getClass(), "LoadFielStatus", "loading...");
        if (cbRememberName.isChecked()) {
            loginName.setText(DataStorage.LoginData.GetLoginUsername());
        } else {
            loginName.setText("");
        }
        if (cbRememberPass.isChecked()) {
            loginPassword.setText(DataStorage.LoginData.GetLoginPassword());
        } else {
            loginPassword.setText("");
        }
        return true;
    }

    public void LoginOnClickEvent(View v) {
        loginName.setText("admin");
        loginPassword.setText("admin");
    }

    public void test() {
        System.out.println("Test");
        PersonResponse.Person person = new PersonResponse().new Person();
        PersonResponse.Boots boots = new PersonResponse().new Boots();
        PersonResponse.Gloves gloves = new PersonResponse().new Gloves();
        PersonResponse.Sword sword = new PersonResponse().new Sword();
        PersonResponse.Shield shield = new PersonResponse().new Shield();
        person.boots = boots;
        person.gloves = gloves;
        person.sword = sword;
        person.shield = shield;
        final Command command = new Command() {
            @Override
            public void execute() {
                service.getInstace().getLatestPerson(new Callback<PersonResponse>() {
                    @Override
                    public void success(PersonResponse personResponse, Response response) {
                        if (personResponse.results != null) {
                            if (personResponse.results.size() > 0) {
                                PersonResponse.Person person1 = personResponse.results.get(0);
                                System.out.println("Person: " + person1.age + ", " + person1.length);
                                if (person1.boots != null)
                                    person1.boots.print();
                                else
                                    System.out.println("No boots");
                                if (person1.gloves != null)
                                    person1.gloves.print();
                                else
                                    System.out.println("No boots");
                                if (person1.sword != null)
                                    person1.sword.print();
                                else
                                    System.out.println("No boots");
                                if (person1.shield != null)
                                    person1.shield.print();
                                else
                                    System.out.println("No boots");
                                return;
                            }
                        }
                        System.out.println("Failed!");
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        System.out.println("Failed!");
                    }
                });
            }
        };
//        service.getInstace().addPerson(person, new Callback<PersonResponse>() {
//            @Override
//            public void success(PersonResponse personResponse, Response response) {
//                System.out.println("Succes!!!");
//                command.execute();
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                System.out.println("Test submission failed");
//            }
//        });
        command.execute();
    }
}
