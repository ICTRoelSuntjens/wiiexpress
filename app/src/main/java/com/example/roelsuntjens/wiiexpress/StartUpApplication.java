package com.example.roelsuntjens.wiiexpress;

import android.app.Application;

import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import java.io.File;

import API.service;

/**
 * Created by roelsuntjens on 18-09-15.
 */
public class StartUpApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        OkHttpClient httpClient = new OkHttpClient();
        try {
            int cacheSize = 10 * 1024 * 1024;
            File cachDirectory = new File(getCacheDir().getAbsoluteFile(), "HttpCache");
            Cache cache = new Cache(cachDirectory, cacheSize);
            httpClient.setCache(cache);
        } catch (Exception e) {
            e.printStackTrace();
        }
        service.setClient(httpClient);
    }
}
