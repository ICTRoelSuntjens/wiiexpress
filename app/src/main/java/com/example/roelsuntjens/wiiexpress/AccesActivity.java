package com.example.roelsuntjens.wiiexpress;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import API.Querries;
import API.service;
import Singleton.DataStorage;
import model.UserData;
import model.UserDataResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AccesActivity extends AppCompatActivity {
    private static String SERVERNOTREACHED = "Could not reach the server";
    private static String LISTUPDATE = "List updated";
    private Button removeAllButton;
    private CheckBox yesToAllCheckbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acces);
        init();
    }

    private void init() {
        initComponents();
        initListView();
    }

    private void initComponents() {
        yesToAllCheckbox = (CheckBox) findViewById(R.id.acces_yes_to_all_cb);
        removeAllButton = (Button) findViewById(R.id.acces_remove_all_bn);
        removeAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeAllUsers(view);
            }
        });
    }

    private void initListView() {
        updateList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_acces, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateList() {
        final ListView listView = (ListView) findViewById(R.id.acces_user_listview);

        service.getInstace().getUsers(new Callback<UserDataResponse>() {
            @Override
            public void success(UserDataResponse userDataResponse, Response response) {
                UserListAdapter adapter = new UserListAdapter(AccesActivity.this, R.layout.user_list_item, userDataResponse.results);
                listView.setAdapter(adapter);
            }

            @Override
            public void failure(RetrofitError error) {
                showToast("Could not load users");
            }
        });
    }

    public void editUserOnClickHandler(View v) {
        ListView listView = (ListView) findViewById(R.id.acces_user_listview);
        final UserListAdapter adapter = (UserListAdapter) listView.getAdapter();
        final UserData userToEdit = (UserData) v.getTag();

        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.edit_window, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);
        final EditText userInput = (EditText) promptsView.findViewById(R.id.editTextDialogUserInput);
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    int newAcces = Integer.parseInt(userInput.getText().toString());
                                    System.out.println("New Acces: " + newAcces);
                                    editUser(userToEdit, newAcces);
                                } catch (Exception e) {
                                    showToast("Use only numbers!");
                                }
//                                result.setText(userInput.getText());
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        //editUser(userToEdit, v, adapter, toChangeAcces);
    }

    public void removeUserOnClickHandler(final View v) {
        ListView listView = (ListView) findViewById(R.id.acces_user_listview);
        final UserListAdapter adapter = (UserListAdapter) listView.getAdapter();
        final UserData userToRemove = (UserData) v.getTag();

        if (((CheckBox) (findViewById(R.id.acces_yes_to_all_cb))).isChecked()) {
            removeUser(userToRemove, v, adapter);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Erase [" + userToRemove.username + "]?")
                    .setMessage("Are your sure?")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            removeUser(userToRemove, v, adapter);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            showToast("Nothing happend");
                        }
                    })
                    .show();
        }
    }

    public void removeAllUsers(View view) {
        final ListView listView = (ListView) findViewById(R.id.acces_user_listview);
        final UserListAdapter adapter = (UserListAdapter) listView.getAdapter();
        for (UserData data : adapter.items) {
            removeUser(data, view, adapter);
        }
    }

    private void editUser(final UserData userToEdit, final int newAcces) {
        System.out.println("Edit user: [" + userToEdit.objectId + "] " + userToEdit.username + " with acces [" + userToEdit.acces + "] to acces [" + newAcces + "]");
        service.getInstace().userByQuery(Querries.getUserExists(userToEdit.username), new Callback<UserDataResponse>() {
                    @Override
                    public void success(UserDataResponse userDataResponse, Response response) {
                        if (userDataResponse.results.size() > 0) {
                            UserData user = userDataResponse.results.get(0);
                            if (!user.username.equals(DataStorage.UserClass.getCurrentUser().username)) {
                                if (user.acces < DataStorage.UserClass.getCurrentUser().acces) {
                                    if (newAcces < DataStorage.UserClass.getCurrentUser().acces) {
                                        user.acces = newAcces;
                                        service.getInstace().setUserAcces(user.objectId, user, new Callback<UserDataResponse>() {
                                            @Override
                                            public void success(UserDataResponse userDataResponse, Response response) {
                                                showToast("User [" + userToEdit.username + "] succesfully edited");
                                                updateList();
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                System.out.println("Editing goes wrong");
                                                showToast(SERVERNOTREACHED);
                                            }
                                        });
                                    } else {
                                        showToast("Only allowed acces lower then your self");
                                    }
                                } else {
                                    showToast("You need higher acces then [" + user.username + "] to edit his fields", Toast.LENGTH_LONG);
                                }
                            } else {
                                showToast("You can not upgrade yourself");
                            }
                        } else {
                            showToast("User [" + userToEdit.username + "] not found");
                            updateList();
                            showToast(LISTUPDATE);
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        showToast(SERVERNOTREACHED);
                    }
                }

        );
    }

    private void removeUser(final UserData userToRemove, final View v, final UserListAdapter adapter) {
        v.animate().setDuration(2000).alpha(0).withEndAction(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
                v.setAlpha(1);
                service.getInstace().userByQuery(Querries.getUserExists(userToRemove.username), new Callback<UserDataResponse>() {
                    @Override
                    public void success(UserDataResponse userDataResponse, Response response) {
                        if (userDataResponse.results.size() == 0) {
                            showToast("User [" + userToRemove + "] could not be found");
                            updateList();
                            showToast(LISTUPDATE);
                            return;
                        }
                        UserData user = userDataResponse.results.get(0);
                        if (DataStorage.UserClass.getCurrentUser() != null) {
                            if (userDataResponse.results.size() > 0) {
                                if (user.editable || (DataStorage.UserClass.getCurrentUser().acces == 99)) {
                                    if (user.acces < DataStorage.UserClass.getCurrentUser().acces || DataStorage.UserClass.getCurrentUser().acces == 99 || user.username.equals(userToRemove.username)) {
                                        service.getInstace().deleteUser(userDataResponse.results.get(0).objectId, new Callback<UserDataResponse>() {
                                            @Override
                                            public void success(UserDataResponse userDataResponse, Response response) {
                                                if (userDataResponse == null) {
                                                    removed();
                                                } else if (userDataResponse.results == null) { // this if gets used
                                                    removed();
                                                } else if (userDataResponse.results.size() == 0) {
                                                    removed();
                                                } else {
                                                    showToast("Could not delete user[" + userToRemove.username + "]");
                                                }
                                            }

                                            private void removed() {
                                                showToast("User[" + userToRemove.username + "] is deleted");
                                                if (DataStorage.UserClass.getCurrentUser() != null)
                                                    if (userToRemove.username.equals(DataStorage.UserClass.getCurrentUser().username)) {
                                                        DataStorage.UserClass.setCurrentUser(null);
                                                        navigateToLogin();
                                                        onBackPressed();
                                                        return;
                                                    }
                                                adapter.remove(userToRemove);
                                            }


                                            @Override
                                            public void failure(RetrofitError error) {
                                                showToast(SERVERNOTREACHED);
                                            }
                                        });
                                    } else {
                                        showToast("Your acces is not higher then [" + user.username + "]");
                                    }

                                } else {
                                    showToast("User [" + user.username + "] not editable");
                                }
                            } else {
                                showToast("User [" + user.username + "] not found");
                                updateList();
                            }
                        } else {
                            showToast("Your login was wrong [null]");
                            navigateToLogin();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        showToast(SERVERNOTREACHED);
                    }
                });
            }
        });
    }

    private void showToast(String message) {
        showToast(message, Toast.LENGTH_SHORT);
    }

    private void showToast(String message, int time) {
        Toast.makeText(AccesActivity.this, message, time).show();
    }

    private void navigateToLogin() {
        startActivity(new Intent(AccesActivity.this, LoginActivity.class));
    }

    public class UserListAdapter extends ArrayAdapter<UserData> {
        private List<UserData> items;
        private int layoutResourceId;
        private Context context;

        public UserListAdapter(Context context, int layoutResourceId, List<UserData> items) {
            super(context, layoutResourceId, items);
            this.context = context;
            this.layoutResourceId = layoutResourceId;
            this.items = items;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            UserHolder userHolder = null;
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            userHolder = new UserHolder();
            userHolder.user = items.get(position);
            userHolder.editUserButton = (ImageButton) row.findViewById(R.id.list_user_edit_button);
            userHolder.removeUserButton = (ImageButton) row.findViewById(R.id.list_user_remove_button);
            userHolder.editable = (CheckBox) row.findViewById(R.id.list_user_editable_cb);
            userHolder.editUserButton.setTag(userHolder.user);
            userHolder.removeUserButton.setTag(userHolder.user);

            userHolder.field1 = (TextView) row.findViewById(R.id.list_user_field1_txt);
            userHolder.field2 = (TextView) row.findViewById(R.id.list_user_field2_txt);

            row.setTag(userHolder);

            setupItem(userHolder);

            return row;
        }

        private void setupItem(UserHolder holder) {
            holder.field1.setText(holder.user.username);
            holder.field2.setText(String.valueOf(holder.user.acces));
            holder.editable.setChecked(holder.user.editable);
        }

        public class UserHolder {
            UserData user;
            TextView field1;
            TextView field2;
            CheckBox editable;
            ImageButton editUserButton;
            ImageButton removeUserButton;
        }
    }
}
