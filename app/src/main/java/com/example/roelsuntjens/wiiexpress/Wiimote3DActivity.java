package com.example.roelsuntjens.wiiexpress;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import View3D.wiimote.WiimoteGLSurfaceView;

public class Wiimote3DActivity extends AppCompatActivity {
    private WiimoteGLSurfaceView glView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        glView = new WiimoteGLSurfaceView(this, Wiimote3DActivity.this);
        this.setContentView(glView);
//        glView.startUpdating();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_wiimote3_d, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_startUpdating) {
            glView.startUpdating();
            return true;
        } else if (id == R.id.action_stopUpdating) {
            glView.stopUpdating();
            return true;
        } else if (id == R.id.action_back) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        glView.stopUpdating();
        startActivity(new Intent(Wiimote3DActivity.this, HomeActivity.class));
    }

    public void showToast(String message) {
        showToast(message, Toast.LENGTH_SHORT);
    }

    public void showToast(final String message, final int time) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(Wiimote3DActivity.this, message, time).show();
            }
        });
    }
}
