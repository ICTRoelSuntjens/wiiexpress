package com.example.roelsuntjens.wiiexpress;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import API.service;
import Singleton.DataStorage;
import custom.live.feed.MyOwnView;
import model.EikDataResponse;
import model.WiiData;
import model.WiiDataResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LiveFeedActivity extends Activity {

    private MyOwnView ownView;
    private Thread updateThread;
    private List<TextView> textViews;
    private WiiData latestWiiData = null;
    private ProgressBar pbar;

    private long pastEikUpdateTime = 0;
    private long eikUpdateDelay = 10000;

    private boolean showShapes = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ownView = new MyOwnView(this);
        ownView.setOrientation(MyOwnView.VERTICAL);
        setContentView(ownView);
//        setContentView(R.layout.activity_live_feed);
        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_live_feed, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.menu_pauze_feed:
                livePauzedOnClick();
                return true;
            case R.id.menu_live_feed:
                liveOnClick();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        stopAll();
//        startActivity(new Intent(LiveFeedActivity.this, HomeActivity.class));
    }

    private void init() {
        initTextViews();
        initProgressbar();
    }

    private void initUpdateThread() {
        this.updateThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (true) {
                        updateValues();
                        Thread.sleep(1000 / 10);
                    }
                } catch (InterruptedException e) {
                    DataStorage.SystemPrint(this.getClass(), "UpdateThread", "Update Thread Interrupted");
                }
            }
        });
    }

    private void initTextViews() {
        textViews = new ArrayList<TextView>();
        textViews.add((TextView) (findViewById(R.id.live_accx)));
        textViews.add((TextView) (findViewById(R.id.live_accy)));
        textViews.add((TextView) (findViewById(R.id.live_accz)));
        textViews.add((TextView) (findViewById(R.id.live_pos1x)));
        textViews.add((TextView) (findViewById(R.id.live_pos1y)));
        textViews.add((TextView) (findViewById(R.id.live_pos1size)));
        textViews.add((TextView) (findViewById(R.id.live_pos2x)));
        textViews.add((TextView) (findViewById(R.id.live_pos2y)));
        textViews.add((TextView) (findViewById(R.id.live_pos2size)));
        textViews.add((TextView) (findViewById(R.id.live_button1)));
        textViews.add((TextView) (findViewById(R.id.live_button2)));
        textViews.add((TextView) (findViewById(R.id.live_buttonA)));
        textViews.add((TextView) (findViewById(R.id.live_buttonB)));
        textViews.add((TextView) (findViewById(R.id.live_buttonMin)));
        textViews.add((TextView) (findViewById(R.id.live_buttonHome)));
        textViews.add((TextView) (findViewById(R.id.live_buttonPlus)));
        textViews.add((TextView) (findViewById(R.id.live_buttonUp)));
        textViews.add((TextView) (findViewById(R.id.live_buttonDown)));
        textViews.add((TextView) (findViewById(R.id.live_buttonLeft)));
        textViews.add((TextView) (findViewById(R.id.live_buttonRight)));
    }

    private void initProgressbar() {
        pbar = (ProgressBar) (findViewById(R.id.live_progressbar));
    }

    private void stopAll() {
        livePauzedOnClick();
    }

    private void repaintLiveFeed() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ownView.update(latestWiiData);
            }
        });
    }

    private void updateValues() {
        long currentTime = System.currentTimeMillis();
        if (pastEikUpdateTime + eikUpdateDelay < currentTime) {
            pastEikUpdateTime = currentTime;
            service.getInstace().getLatestEikData(new Callback<EikDataResponse>() {
                @Override
                public void success(EikDataResponse eikDataResponse, Response response) {
                    if (eikDataResponse.results.size() > 0) {
                        ownView.setEikData(eikDataResponse.results.get(0));
                    } else {
//                        Toast.makeText(LiveFeedActivity.this, "No Eik Data Found (" + eikUpdateDelay / 1000 + " sec)", Toast.LENGTH_SHORT);
                        MyOwnView.FEEDBACK = "No Eik Data Found (" + eikUpdateDelay / 1000 + " sec)";
                    }
                }

                @Override
                public void failure(RetrofitError error) {
//                    Toast.makeText(LiveFeedActivity.this, "No connection to the server", Toast.LENGTH_SHORT).show();
                    MyOwnView.FEEDBACK = "No connection to the server";
                }
            });
        }
        service.getInstace().getLatestData(new Callback<WiiDataResponse>() {
            @Override
            public void success(WiiDataResponse wiiDataResponse, Response response) {
                if (wiiDataResponse.results.size() > 0) {
                    latestWiiData = wiiDataResponse.results.get(0);
                    repaintLiveFeed();
                } else {
//                    Toast.makeText(LiveFeedActivity.this, "There is no data in the database", Toast.LENGTH_SHORT).show();
                    MyOwnView.FEEDBACK = "There is no data in the database";
                    System.out.println("No data in database");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ownView.updateBizzyLed(false);
                        }
                    });
                }
            }

            @Override
            public void failure(RetrofitError error) {
                DataStorage.SystemPrint(this.getClass(), "updateValues", "Update values = failed");
//                Toast.makeText(LiveFeedActivity.this, "No connection to the server", Toast.LENGTH_SHORT).show();
                MyOwnView.FEEDBACK = "No connection to the server";

            }
        });
    }

    private void updateTextViews() {
        if (latestWiiData != null) {
            int i = 0;
            textViews.get(i++).setText("Accx: " + latestWiiData.accx);
            textViews.get(i++).setText("Accy: " + latestWiiData.accy);
            textViews.get(i++).setText("Accz: " + latestWiiData.accz);
            textViews.get(i++).setText("Pos 1 x: " + latestWiiData.pos1x);
            textViews.get(i++).setText("Pos 1 y: " + latestWiiData.pos1y);
            textViews.get(i++).setText("Pos 1 size: " + latestWiiData.size1);
            textViews.get(i++).setText("Pos 2 x: " + latestWiiData.pos2x);
            textViews.get(i++).setText("Pos 2 y: " + latestWiiData.pos2y);
            textViews.get(i++).setText("Pos 2 size: " + latestWiiData.size2);
            textViews.get(i++).setText("Button 1: " + latestWiiData.button1);
            textViews.get(i++).setText("Button 2: " + latestWiiData.button2);
            textViews.get(i++).setText("Button A: " + latestWiiData.buttonA);
            textViews.get(i++).setText("Button B: " + latestWiiData.buttonB);
            textViews.get(i++).setText("Button Min: " + latestWiiData.buttonMin);
            textViews.get(i++).setText("Button Home: " + latestWiiData.buttonHome);
            textViews.get(i++).setText("Button Plus: " + latestWiiData.buttonPlus);
            textViews.get(i++).setText("Button Up: " + latestWiiData.buttonUp);
            textViews.get(i++).setText("Button Down: " + latestWiiData.buttonDown);
            textViews.get(i++).setText("Button Left: " + latestWiiData.buttonLeft);
            textViews.get(i++).setText("Button Right: " + latestWiiData.buttonRight);
        }
    }

    public void livePauzedOnClick() {
        if (updateThread != null) {
            updateThread.interrupt();
            updateThread = null;
        }
    }

    public void liveOnClick() {
        if (updateThread == null) {
            initUpdateThread();
            updateThread.start();
        }
    }
}
