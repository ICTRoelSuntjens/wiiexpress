package com.example.roelsuntjens.wiiexpress;

import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import API.service;
import View3D.GLRenderer;
import View3D.Pong.PongGLRenderer;
import View3D.ShootingShip.ShootingShipGLRenderer;
import model.Command;
import model.EikData;
import model.EikDataResponse;
import model.WiiData;
import model.WiiDataResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PlatformActivity extends Activity {
    public static final int ShootingShip = 1;
    public static final int Pong = 2;
    private int platform = -1;
    private GLSurfaceView glView;
    private GLRenderer renderer;
    private Thread updateThread, onlineThread;
    private long currentTime = 0, lastEikTime = 0, eikDelay = 10 * 1000;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        glView = new GLSurfaceView(this);
        platform = getIntent().getIntExtra("Platform", -1);
        switch (platform) {
            case ShootingShip:
                renderer = new ShootingShipGLRenderer();
                break;
            case Pong:
                renderer = new PongGLRenderer(this);
                break;
        }
        glView.setRenderer(renderer);
        setContentView(glView);
//        stopUpdatingThread();
//        stopOnlineThread();
        startUpdatingThread();
        startOnlineThread();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        stopOnlineThread();
        stopUpdatingThread();
    }

    private void initUpdateThread() {
        this.updateThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (true) {
                        switch (platform) {
                            case ShootingShip:
                                updateShootingShip((ShootingShipGLRenderer) renderer);
                                break;
                            case Pong:
                                updatePong((PongGLRenderer) renderer);
                                break;
                        }
                        Thread.sleep(1000 / 30);
                    }
                } catch (InterruptedException e) {
                    Log.d("InitUpdateThread", "Update Thread Interrupted");
                }
            }
        });
    }

    private void initOnlineThread() {
        this.onlineThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (true) {
                        updateWiiData(new Command() {
                            @Override
                            public void execute() {
                                if (lastEikTime + eikDelay < currentTime) {
                                    lastEikTime = currentTime;
                                    updateEikData(currentTime, null);
                                }
                            }
                        });
                        Thread.sleep(1000 / 10);
                    }
                } catch (InterruptedException e) {

                }
            }
        });
    }

    public void startUpdatingThread() {
        if (updateThread == null) {
            initUpdateThread();
            updateThread.start();
        }
    }

    public void stopUpdatingThread() {
        if (updateThread != null) {
            updateThread.interrupt();
            updateThread = null;
        }
    }

    public void startOnlineThread() {
        if (onlineThread == null) {
            initOnlineThread();
            onlineThread.start();
        }
    }

    public void stopOnlineThread() {
        if (onlineThread != null) {
            onlineThread.interrupt();
            onlineThread = null;
        }
    }

    private void updatePong(final PongGLRenderer renderer) {
        currentTime = System.currentTimeMillis();
        renderer.update(currentTime);
    }

    private void updateShootingShip(final GLRenderer renderer) {
        currentTime = System.currentTimeMillis();
        renderer.update(currentTime);
    }

    private void updateWiiData(final Command command) {
        service.getInstace().getLatestData(new Callback<WiiDataResponse>() {
            @Override
            public void success(WiiDataResponse wiiDataResponse, Response response) {
                if (wiiDataResponse.results.size() > 0) {
                    renderer.setWiiData(wiiDataResponse.results.get(0));
                    if (command != null)
                        command.execute();
                } else {
                    renderer.setWiiData(null);
                    showToast("No Wii Data Found");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                WiiData data = new WiiData();
                data.accx = 119;
                data.accy = 120;
                data.accz = 144;
                EikData eik = new EikData();
                eik.eikx = 119;
                eik.eiky = 120;
                eik.eikz = 144;
                renderer.setWiiData(data);
                renderer.setEikData(eik);
                System.out.println("Update Wii Data failed: Reason= " + error.getMessage());
                showToast("Update Failed");
            }
        });
    }

    private void updateEikData(final long currentTime, final Command command) {
        service.getInstace().getLatestEikData(new Callback<EikDataResponse>() {
            @Override
            public void success(EikDataResponse eikDataResponse, Response response) {
                if (eikDataResponse.results.size() > 0) {
                    renderer.setEikData(eikDataResponse.results.get(0));
                    renderer.update(currentTime);
                    if (command != null)
                        command.execute();
                } else {
                    showToast("No Eik Data Found");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                showToast("Eik Failed!");
            }
        });
    }

    private void showToast(String message) {
        Toast.makeText(PlatformActivity.this, message, Toast.LENGTH_SHORT).show();
    }
}
