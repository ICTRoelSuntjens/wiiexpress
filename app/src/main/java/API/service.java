package API;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.Date;

import model.AboutDataResponse;
import model.EikDataResponse;
import model.PersonResponse;
import model.UserData;
import model.UserDataResponse;
import model.WiiData;
import model.WiiDataResponse;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.HEAD;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by roelsuntjens on 18-09-15.
 */
public class service {

    private static Service service = null;
    private static OkHttpClient client = null;
    private static RestAdapter adapter = null;
    private static String APPLICATIONID = "YGUDFfarP0MFXnoHlAdOee2OcfZ01QI4YgxisyNa";
    private static String RESTAPIKEY = "lrJv1I4Ow2jqgeR5Ovo8HObcMFCbCEVzazhYCBQL";
    //    private static String MASTERKEY = "7z0YdmBUP0m1DkYWaV0vzbirMEzatf8QlFhIKPdm";
    private static String TYPE = "application/json";
    private static String API = "https://api.parse.com/1";

    public static Service getInstace() {
        if (service == null) {
            client.interceptors().add(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request request = chain.request()
                            .newBuilder()
                            .addHeader("X-Parse-Application-Id", APPLICATIONID)
                            .addHeader("X-Parse-REST-API-Key", RESTAPIKEY)
//                            .addHeader("X-Parse-Master-Key", MASTERKEY)
                            .addHeader("Content-Type", TYPE)
                            .build();
                    return chain.proceed(request);
                }
            });
            OkClient okClient = new OkClient(client);
            adapter = new RestAdapter.Builder().setEndpoint(API).setLogLevel(RestAdapter.LogLevel.NONE).setClient(okClient).build();
            service = adapter.create(Service.class);
        }
        return service;
    }

    public static void setClient(OkHttpClient httpClient) {
        client = httpClient;
    }

    public interface Service {

        /*
        * Wii Data
         */
        @GET("/classes/WiiData/{key}")
        public void getWiiData(@Path("key") String key, Callback<WiiData> response);

        @GET("/classes/WiiData/?limit=1&order=-updatedAt")
        public void getWiiData(Callback<WiiDataResponse> response);

        @GET("/classes/WiiData")
        public void list(@Query(value = "order%20by", encodeName = false) String order, Callback<WiiDataResponse> response);

        @GET("/classes/WiiData/?sort=desc")
        public void getHEAD(@Query("updatedAt") Date date, Callback<WiiDataResponse> response);

        @HEAD("/classes/WiiData")
        public void getHEAD(Callback<WiiData> response);

        @GET("/classes/WiiData?limit=1&order=-updatedAt")
        public void getLatestData(Callback<WiiDataResponse> response);

        @GET("/classes/WiiData?limit={limit}&skip={skip}")
        public void getThousandResults(@Query("limit") int limit, @Query("skip") int skip, Callback<WiiDataResponse> response);

        @GET("/classes/WiiData")
        public void wiiDataByQuery(@Query("where") String query, Callback<WiiDataResponse> response);

        /*
        * User Data
         */
        @GET("/classes/User")
        public void getUsers(Callback<UserDataResponse> response);

        @DELETE("/classes/User")
        public void deleteAllUsers(Callback<UserDataResponse> response);

        @DELETE("/classes/User/{objectId}")
        public void deleteUser(@Path("objectId") String objectId, Callback<UserDataResponse> response);

        @POST("/classes/User/")
        public void addUser(@Body UserData user, Callback<UserDataResponse> response);

        @GET("/classes/User/")
        public void userByQuery(@Query("where") String query, Callback<UserDataResponse> response);

        @PUT("/classes/User/{objectId}")
        public void setUserAcces(@Path("objectId") String objectId, @Body UserData user, Callback<UserDataResponse> response);

        /*
         * About Data
          */
        @GET("/classes/AboutPage/")
        public void aboutByQuery(@Query("where") String query, Callback<AboutDataResponse> response);

        @GET("/classes/AboutPage/")
        public void getAllColumns(Callback<AboutDataResponse> response);

        /*
        * Eik Data
         */
        @GET("/classes/WiiEikAcc?limit=1&order=-updatedAt")
        public void getLatestEikData(Callback<EikDataResponse> response);

        /**
         * Test data
         */
        @POST("/classes/Persons")
        public void addPerson(@Body PersonResponse.Person person, Callback<PersonResponse> response);

        @GET("/classes/Persons?limit=1&order=-updatedAt")
        public void getLatestPerson(Callback<PersonResponse> response);
    }
}
