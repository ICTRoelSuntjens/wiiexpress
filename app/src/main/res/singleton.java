/**
 * Created by roelsuntjens on 08-09-15.
 */
public class singleton {
    private static singleton ourInstance = new singleton();

    public static singleton getInstance() {
        return ourInstance;
    }

    private singleton() {
        System.out.println("Create Singleton");
    }


}
